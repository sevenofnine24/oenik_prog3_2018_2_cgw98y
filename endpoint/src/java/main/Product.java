/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author benceb
 */
@XmlRootElement
public class Product {
    String name;
    int stock;

    public Product(String name, int stock) {
        this.name = name;
        this.stock = stock;
    }
    
    public Product() {
        
    }
    
    @XmlElement
    public String getName() {
        return name;
    }

    @XmlElement
    public int getStock() {
        return stock;
    }
}
