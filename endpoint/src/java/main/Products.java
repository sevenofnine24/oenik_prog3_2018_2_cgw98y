/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.util.ArrayList;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author benceb
 */
@XmlRootElement()
public class Products {
    ArrayList<Product> list;

    public Products(String getString) {
        list = new ArrayList();
        String[] segments = getString.split(",");
        
        for (String segment : segments) {
            Product toadd = new Product();
            toadd.name = segment;
            toadd.stock = RandomGenerator.Generate(100);
            if (!segment.isEmpty()) {
                list.add(toadd);
            }
        }
    }
    
    public Products() {
        
    }

    @XmlElement(name="product")
    public ArrayList<Product> getList() {
        return list;
    }
}
