# Apple Store

##Táblák:

* Termékek (id [key], név, kategória [foreign], rövid leírás, alapár, megjelenés dátuma)
* Raktár (id [key], termék [foreign], szín [foreign], tárhely, darab, beérkezés dátuma)
* Rendelések (id [key], név + tárhely, darab, összár, rendelő id [foreign])
* Rendelők tábla (id [key], név, cím, telefonszám)
* Termékkategória kapcsolótábla (id [key], név)
* Szín kapcsolótábla (id [key], név)
* Tárhely ár kapcsolótábla (tárhely [key], extra ár)
	
##Funkciók:

* CRUD a 3 fő táblára
* Egyes rendelők rendeléseinek össz. értéke
* Raktáron lévő termékek színe, tárhelye, és teljes ára
* Átlagos alap ár termékkategóriánként
* Java webes végponttal külső raktár státusz egy termékre