﻿// <copyright file="SqlRepositoryTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace AppleStore.Repository.Tests
{
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using AppleStore.Data;
    using AppleStore.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Tests for SQL repository
    /// </summary>
    [TestFixture]
    internal class SqlRepositoryTests
    {
        /// <summary>
        /// Creates mock context
        /// </summary>
        /// <returns>Mocked context</returns>
        public Mock<DatabaseEntities> MockContextFactory()
        {
            Mock<DatabaseEntities> ctx = new Mock<DatabaseEntities>();

            return ctx;
        }
    }
}
