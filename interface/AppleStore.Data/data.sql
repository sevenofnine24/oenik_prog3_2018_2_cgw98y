﻿CREATE TABLE termekkategoria (
	id int not null,
	nev varchar(30),
	CONSTRAINT tk_pk PRIMARY KEY (id)
);
CREATE TABLE szin (
	id int not null,
	nev varchar(30),
	CONSTRAINT szin_pk PRIMARY KEY (id)
);
CREATE TABLE tarhely_ar (
	tarhely varchar(10) not null,
	ar int,
	CONSTRAINT tarhely_ar_pk PRIMARY KEY (tarhely)
);
CREATE TABLE termekek (
	id int not null,
	nev varchar(50),
	kategoria int,
	leiras varchar(200),
	ar int,
	megjelenes date,
	CONSTRAINT termekek_pk PRIMARY KEY (id),
	CONSTRAINT kategoria_fk FOREIGN KEY (kategoria) REFERENCES termekkategoria(id)
);
CREATE TABLE raktar (
	id int not null,
	termek int,
	szin int,
	tarhely varchar(10),
	darab int,
	beerkezes date
	CONSTRAINT raktar_pk PRIMARY KEY (id),
	CONSTRAINT termek_fk FOREIGN KEY (termek) REFERENCES termekek(id),
	CONSTRAINT szin_fk FOREIGN KEY (szin) REFERENCES szin(id),
	CONSTRAINT tarhely_fk FOREIGN KEY (tarhely) REFERENCES tarhely_ar(tarhely)
);
CREATE TABLE rendelok (
	nev varchar(50) not null,
	cim varchar(50) not null,
	telefonszam int
	CONSTRAINT rendelok_pk PRIMARY KEY (nev, cim)
);
CREATE TABLE  rendelesek (
	id int not null,
	termek varchar(50),
	darab int,
	osszar int,
	rendelo_neve varchar(50),
	rendelo_cime varchar(50),
	CONSTRAINT rendelesek_pk PRIMARY KEY (id),
	CONSTRAINT rendelo_fk FOREIGN KEY (rendelo_neve, rendelo_cime) REFERENCES rendelok(nev, cim)
); 

INSERT INTO termekkategoria VALUES (0, 'iPhone');
INSERT INTO termekkategoria VALUES (1, 'iPad');
INSERT INTO termekkategoria VALUES (2, 'Mac');
INSERT INTO termekkategoria VALUES (3, 'Watch');
INSERT INTO termekkategoria VALUES (4, 'Kiegészítők');

INSERT INTO szin VALUES (0, 'Space Grey');
INSERT INTO szin VALUES (1, 'Silver');
INSERT INTO szin VALUES (2, 'Gold');
INSERT INTO szin VALUES (3, 'Rose Gold');
INSERT INTO szin VALUES (4, 'Product RED');

INSERT INTO tarhely_ar VALUES ('64GB', 0);
INSERT INTO tarhely_ar VALUES ('128GB', 40000);
INSERT INTO tarhely_ar VALUES ('256GB', 80000);
INSERT INTO tarhely_ar VALUES ('512GB', 120000);
INSERT INTO tarhely_ar VALUES ('1TB', 200000);
INSERT INTO tarhely_ar VALUES ('2TB', 300000);

INSERT INTO termekek VALUES (0, 'iPhone 7', 0, 'IP67 vízállóság, új design', 160000, convert(date, '2016.09.01', 102));
INSERT INTO termekek VALUES (1, 'iPhone 7 Plus', 0, 'IP67 vízállóság, új design', 190000, convert(date, '2016.09.01', 102));	
INSERT INTO termekek VALUES (2, 'iPhone 8', 0, 'Qi töltés, A11 bionic chip', 200000, convert(date, '2017.09.01', 102));
INSERT INTO termekek VALUES (3, 'iPhone 8 Plus', 0, 'Qi töltés, A11 bionic chip', 230000, convert(date, '2017.09.01', 102));
INSERT INTO termekek VALUES (4, 'iPhone X', 0, 'Super Retina kijelző, Face ID', 300000, convert(date, '2017.09.01', 102));
INSERT INTO termekek VALUES (5, 'iPhone XS', 0, 'A12 bionic chip, sztereó videó felvétel', 360000, convert(date, '2018.09.01', 102));
INSERT INTO termekek VALUES (6, 'iPhone XS Max', 0, 'A12 bionic chip, sztereó videó felvétel', 400000, convert(date, '2018.09.01', 102));
INSERT INTO termekek VALUES (7, 'iPad 2018', 1, 'Apple Pencil támogatás', 100000, convert(date, '2018.09.01', 102));
INSERT INTO termekek VALUES (8, 'iPad Pro 10.5 inch 2017', 1, 'Nagyobb kijelző, erősebb chip', 210000, convert(date, '2017.06.01', 102));
INSERT INTO termekek VALUES (9, 'iPad Pro 12.9 inch 2017', 1, 'Nagyobb kijelző, erősebb chip', 210000, convert(date, '2017.06.01', 102));
INSERT INTO termekek VALUES (10, 'MacBook Pro 13 inch', 2, 'TouchBar, halkabb billentyűzet', 500000, convert(date, '2018.06.01', 102));
INSERT INTO termekek VALUES (11, 'MacBook Pro 15 inch', 2, 'TouchBar, halkabb billentyűzet', 800000, convert(date, '2018.06.01', 102));
INSERT INTO termekek VALUES (12, 'MacBook', 2, 'Legvékonyabb MacBook, 10 órás akkumlátor', 400000, convert(date, '2017.06.01', 102));
INSERT INTO termekek VALUES (13, 'Watch Series 3', 3, 'Hívások az iPhone nélkül is', 100000, convert(date, '2017.09.01', 102));
INSERT INTO termekek VALUES (14, 'Watch Series 4', 3, 'Nagyobb kijelző, vékonyabb', 150000, convert(date, '2018.09.01', 102));
INSERT INTO termekek VALUES (15, 'Apple Pencil', 4, 'Pontosabb rajzolás az iPadon', 30000, convert(date, '2015.10.01', 102));
INSERT INTO termekek VALUES (16, 'AirPods', 4, 'Vezeték nélküli jövő', 60000, convert(date, '2016.12.01', 102));

INSERT INTO raktar VALUES (0, 1, 0, '64GB', 2, convert(date, '2018.07.12', 102));
INSERT INTO raktar VALUES (1, 2, 2, '256GB', 4, convert(date, '2018.08.21', 102));
INSERT INTO raktar VALUES (2, 4, 1, '64GB', 12, convert(date, '2018.08.14', 102));
INSERT INTO raktar VALUES (3, 6, 0, '256GB', 3, convert(date, '2018.10.11', 102));
INSERT INTO raktar VALUES (4, 10, 1, '512GB', 4, convert(date, '2018.06.10', 102));

INSERT INTO rendelok VALUES ('Budavári Bence', '1033 Budapest X utca', 123456789);
INSERT INTO rendelok VALUES ('Veres Máté', '2049 Diósd Y utca', 321654987);
INSERT INTO rendelok VALUES ('Madocsay Vilmos', '2030 Érd Z utca', 45689123);
INSERT INTO rendelok VALUES ('Varga Bence', '2440 Százhalombatta W utca', 789456123);

INSERT INTO rendelesek VALUES (0, 'iPhone 8 64GB', 3, 200000, 'Veres Máté', '2049 Diósd Y utca');
INSERT INTO rendelesek VALUES (1, 'iPhone X 64GB', 1, 300000, 'Budavári Bence', '1033 Budapest X utca');
INSERT INTO rendelesek VALUES (2, 'AirPods', 2, 120000, 'Varga Bence', '2440 Százhalombatta W utca');
INSERT INTO rendelesek VALUES (3, 'iPad 2018', 1, 1200000, 'Madocsay Vilmos', '2030 Érd Z utca');
INSERT INTO rendelesek VALUES (4, 'Apple Pencil', 2, 30000, 'Madocsay Vilmos', '2030 Érd Z utca');
