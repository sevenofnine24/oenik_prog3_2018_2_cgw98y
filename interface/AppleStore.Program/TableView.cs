﻿// <copyright file="TableView.cs" company="Apple Inc.">
// All rights reserved. © 2018
// </copyright>

namespace AppleStore.Program
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Reflection;
    using AppleStore.Logic;
    using Menu;

    /// <summary>
    /// Makes a table view with entities
    /// </summary>
    /// <typeparam name="TEntity">Entities to be displayed</typeparam>
    public class TableView<TEntity>
        where TEntity : class, new()
    {
        /// <summary>
        /// TEntity's logic class
        /// </summary>
        private ILogic<TEntity> logic;

        /// <summary>
        /// Stores the menu that will be displayed
        /// </summary>
        private MenuClass menu;

        /// <summary>
        /// Exit token to pass menu object
        /// </summary>
        private bool exitToken = false;

        /// <summary>
        /// Translator for displaying strings
        /// </summary>
        private Translator translator;

        /// <summary>
        /// Initializes a new instance of the <see cref="TableView{TEntity}"/> class.
        /// </summary>
        /// <param name="logic">ILogic of entities</param>
        public TableView(ILogic<TEntity> logic)
        {
            this.translator = new Translator();
            this.logic = logic;
            this.MakeMenu();
        }

        /// <summary>
        /// Starts the table view menu
        /// </summary>
        public void Start()
        {
            this.exitToken = false;
            this.menu.Start(ref this.exitToken);
        }

        /// <summary>
        /// Opens an editor menu then saves the results
        /// </summary>
        /// <param name="entity">Entity to be edited</param>
        public void Editor(TEntity entity)
        {
            Editor<TEntity> editor = new Editor<TEntity>(this.logic, entity, this.translator);
            editor.Start();

            // Reload menu afterwards
            this.Reload();
        }

        /// <summary>
        /// Create a new entity
        /// </summary>
        public void CreateEditor()
        {
            Editor<TEntity> editor = new Editor<TEntity>(this.logic, this.translator);
            editor.Start();

            this.Reload();
        }

        /// <summary>
        /// Reload menu
        /// </summary>
        public void Reload()
        {
            this.exitToken = true;
            this.MakeMenu();
            this.Start();
        }

        /// <summary>
        /// Exits menu and goes back
        /// </summary>
        public void Back()
        {
            this.exitToken = true;
        }

        /// <summary>
        /// Constructs a menu with entities and the format of the entity type.
        /// </summary>
        private void MakeMenu()
        {
            IEnumerable<KeyValuePair<object[], TEntity>> formatted = this.logic.Formatted();
            int[] maxes = new int[formatted.Count()];
            int index;
            foreach (KeyValuePair<object[], TEntity> kvp in formatted)
            {
                index = 0;
                foreach (object value in kvp.Key)
                {
                    int len = value.ToString().Length;
                    if (maxes[index] < len)
                    {
                        maxes[index] = len;
                    }

                    index++;
                }
            }

            List<MenuFunction<TEntity>> functions = new List<MenuFunction<TEntity>>();
            foreach (KeyValuePair<object[], TEntity> kvp in formatted)
            {
                string itemtitle = string.Empty;
                index = 0;
                foreach (object value in kvp.Key)
                {
                    itemtitle += this.FillSpaces(value.ToString(), maxes[index++] + 2);
                }

                functions.Add(new MenuFunction<TEntity>(itemtitle, this.Editor, kvp.Value));
            }

            string header = string.Empty;
            index = 0;
            foreach (string heading in this.logic.Headings)
            {
                header += this.FillSpaces(this.translator.Translate(heading), maxes[index++] + 2);
            }

            IMenuItem[] interactions = new IMenuItem[]
            {
                new MenuAction("Új elem", this.CreateEditor),
                new MenuAction("Vissza", this.Back)
            };
            MenuSection[] sections = new MenuSection[]
            {
                new MenuSection(header, functions.ToArray()),
                new MenuSection("Interakció", interactions)
            };

            this.menu = new MenuClass(sections, this.logic.Title, "Balra / jobbra nyíl: előző / következő oldal\nEnter: szerkesztés vagy törlés");
        }

        /// <summary>
        /// Fills string with spaces
        /// </summary>
        /// <param name="tofill">String to be filled</param>
        /// <param name="desiredLength">Desired full length of string</param>
        /// <returns>Filled string</returns>
        private string FillSpaces(string tofill, int desiredLength)
        {
            for (int i = tofill.Length; i < desiredLength; i++)
            {
                tofill += " ";
            }

            return tofill;
        }
    }
}
