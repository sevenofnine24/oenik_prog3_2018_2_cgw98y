﻿// <copyright file="Program.cs" company="Apple Inc.">
// All rights reserved. © 2018
// </copyright>

namespace AppleStore.Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using AppleStore.Data;
    using AppleStore.Logic;
    using AppleStore.Repository;
    using Menu;

    /// <summary>
    /// Entry point of program
    /// Launches root menu
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// Repository type to be used
        /// </summary>
        private static readonly Type RepoType = typeof(SqlRepository<>);

        /// <summary>
        /// When set to true, program exits.
        /// </summary>
        private static bool exitToken = false;

        /// <summary>
        /// Enters main menu
        /// </summary>
        private static void Main()
        {
            Console.CursorVisible = false;
            IMenuItem[] tables = new IMenuItem[]
            {
                new MenuAction("Termékek tábla", EnterTable<ProductsLogic, Products>),
                new MenuAction("Raktár tábla", EnterTable<StockLogic, Stock>),
                new MenuAction("Rendelések tábla", EnterTable<OrdersLogic, Orders>)
            };
            IMenuItem[] queries = new IMenuItem[]
            {
                new MenuAction("Egyes rendelők rendeléseinek össz. értéke", SumPerCustomer),
                new MenuAction("Raktáron lévő termékek színe, tárhelye, és teljes ára", StockPrice),
                new MenuAction("Átlagos alap ár termékkategóriánként", AveragePricePerCategory)
            };
            IMenuItem[] interactions = new IMenuItem[]
            {
                new MenuFunction<ProductsLogic>("Termék külső raktáron elérhetősége", RemoteStock.ChooseProduct, (ProductsLogic)GetLogic<ProductsLogic>()),
                new MenuAction("Kilépés", Exit)
            };
            MenuSection[] sections = new MenuSection[]
            {
                new MenuSection(
                    "Táblák",
                    tables),
                new MenuSection(
                    "Lekérdezések",
                    queries),
                new MenuSection(
                    "Interakció",
                    interactions)
            };

            MenuClass menu = new MenuClass(sections, "Főmenü");
            menu.Start(ref exitToken);
        }

        /// <summary>
        /// Enters table view of entity type
        /// </summary>
        /// <typeparam name="TLogic">Logic type</typeparam>
        /// <typeparam name="TEntity">Entity type</typeparam>
        private static void EnterTable<TLogic, TEntity>()
            where TEntity : class, new()
        {
            object logic = GetLogic<TLogic>();
            TableView<TEntity> tableview = new TableView<TEntity>((ILogic<TEntity>)logic);

            tableview.Start();
        }

        /// <summary>
        /// Gets logic for a specified repository type
        /// </summary>
        /// <typeparam name="TLogic">Type of logic</typeparam>
        /// <returns>Logic object</returns>
        private static object GetLogic<TLogic>()
        {
            ParameterInfo[] param = typeof(TLogic).GetConstructors()[0].GetParameters();
            object[] repos = new object[param.Length];
            for (int i = 0; i < param.Length; i++)
            {
                Type[] t = param[i].ParameterType.GetGenericArguments();
                Type repo = RepoType.MakeGenericType(t);
                repos[i] = Activator.CreateInstance(repo);
            }

            return Activator.CreateInstance(typeof(TLogic), repos);
        }

        /// <summary>
        /// Sum per customer feature
        /// </summary>
        private static void SumPerCustomer()
        {
            using (OrdersLogic logic = (OrdersLogic)GetLogic<OrdersLogic>())
            {
                IQueryable<SumHelper> result = logic.SumPerCustomer();
                Console.Clear();
                foreach (SumHelper row in result)
                {
                    Console.WriteLine("Név: {0}\nCím: {1}\nÖsszes rendelés: {2}\n", row.CustomerName, row.CustomerAddress, row.Sum);
                }
            }

            Console.ReadLine();
        }

        /// <summary>
        /// Stock full price feature
        /// </summary>
        private static void StockPrice()
        {
            using (StockLogic logic = (StockLogic)GetLogic<StockLogic>())
            {
                IQueryable<StockPriceHelper> result = logic.StockPrice();
                Console.Clear();
                foreach (StockPriceHelper row in result)
                {
                    Console.WriteLine(
                        "Név: {0}\nSzín: {1}\nTárhely: {2}\nAlapár: {3}\nTeljes ár: {4}\n",
                        row.Name,
                        row.Color,
                        row.Storage,
                        row.BasePrice,
                        row.Price);
                }
            }

            Console.ReadLine();
        }

        /// <summary>
        /// Average price per category feature
        /// </summary>
        private static void AveragePricePerCategory()
        {
            using (ProductsLogic logic = (ProductsLogic)GetLogic<ProductsLogic>())
            {
                IEnumerable<KeyValuePair<string, int?>> result = logic.AveragePricePerCategory();
                Console.Clear();
                foreach (KeyValuePair<string, int?> row in result)
                {
                    Console.WriteLine("{0}: {1} Ft", row.Key, row.Value);
                }
            }

            Console.ReadLine();
        }

        /// <summary>
        /// Self explanatory
        /// </summary>
        private static void NotImplemented()
        {
            Console.Clear();
            Console.WriteLine("Ez még nincs kész");
            Console.ReadLine();
        }

        /// <summary>
        /// Sets exit token to true, exiting the program
        /// </summary>
        private static void Exit()
        {
            exitToken = true;
        }
    }
}
