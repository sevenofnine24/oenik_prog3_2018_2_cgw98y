﻿// <copyright file="Translator.cs" company="Apple Inc.">
// All rights reserved. © 2018
// </copyright>

namespace AppleStore.Program
{
    using System.Collections.Generic;
    using System.IO;

    /// <summary>
    /// Translates strings
    /// </summary>
    public class Translator
    {
        /// <summary>
        /// All keys and values from file
        /// </summary>
        private Dictionary<string, string> translator;

        /// <summary>
        /// Initializes a new instance of the <see cref="Translator"/> class.
        /// Reads file and sets up translator dictionary
        /// </summary>
        /// <param name="filename">File to be read</param>
        public Translator(string filename = "hu.txt")
        {
            this.translator = new Dictionary<string, string>();
            using (StreamReader reader = new StreamReader(filename))
            {
                while (!reader.EndOfStream)
                {
                    string[] segments = reader.ReadLine().Split(':');
                    this.translator.Add(segments[0], segments[1]);
                }
            }
        }

        /// <summary>
        /// Translates a string by key
        /// </summary>
        /// <param name="key">Key to be translated</param>
        /// <returns>Translated string</returns>
        public string Translate(string key)
        {
            if (this.translator.ContainsKey(key))
            {
                return this.translator[key];
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Translates multiple keys
        /// </summary>
        /// <param name="keys">IEnumerable of keys</param>
        /// <returns>Translated dictionary</returns>
        public Dictionary<string, string> TranslateBatch(IEnumerable<string> keys)
        {
            Dictionary<string, string> toReturn = new Dictionary<string, string>();
            foreach (string key in keys)
            {
                if (this.translator.ContainsKey(key))
                {
                    toReturn.Add(key, this.translator[key]);
                }
            }

            return toReturn;
        }
    }
}
