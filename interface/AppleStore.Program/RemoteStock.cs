﻿// <copyright file="RemoteStock.cs" company="Apple Inc.">
// All rights reserved. © 2018
// </copyright>

namespace AppleStore.Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Xml;
    using AppleStore.Logic;
    using Menu;

    /// <summary>
    /// Deals with request to Java endpoint
    /// </summary>
    internal class RemoteStock
    {
        /// <summary>
        /// Exit token for menu
        /// </summary>
        private static bool exitToken = false;

        /// <summary>
        /// Opens menu of products
        /// </summary>
        /// <param name="logic">Logic for products</param>
        public static void ChooseProduct(ProductsLogic logic)
        {
            IEnumerable<MenuChoice> names = logic.All().ToList().Select(x => new MenuChoice(x.Name));
            MenuSection section = new MenuSection("Termékek", names.ToArray(), 10, true);

            IMenuItem[] interacitems = new IMenuItem[]
            {
                new MenuFunction<IEnumerable<string>>("Lekérés", Get, section.Choices.Select(x => x.Title)),
                new MenuAction("Vissza", Back)
            };
            MenuSection interaction = new MenuSection("Interakció", interacitems);

            MenuClass menu = new MenuClass(new MenuSection[] { section, interaction });
            menu.Start(ref exitToken);
            exitToken = false;
            logic.Dispose();
        }

        /// <summary>
        /// Gets remote stock and outputs to console
        /// </summary>
        /// <param name="products">Products to get stock of</param>
        private static void Get(IEnumerable<string> products)
        {
            Console.Clear();
            Back();

            string querystr = string.Empty;
            foreach (string product in products)
            {
                querystr += product + ",";
            }

            string response = string.Empty;
            WebClient wc = new WebClient();
            try
            {
                response = wc.DownloadString("http://localhost:8080/endpoint?products=" + querystr);
            }
            catch (WebException)
            {
                Console.WriteLine("A külső raktár szerver nem eléhertő.");
            }
            finally
            {
                wc.Dispose();
            }

            if (response != string.Empty)
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(response);

                Console.WriteLine("Távoli raktár:\n");
                foreach (XmlNode node in doc.DocumentElement)
                {
                    Console.WriteLine("{0}: {1} darab", node.SelectSingleNode("./name").InnerText, node.SelectSingleNode("./stock").InnerText);
                }
            }

            Console.ReadLine();
        }

        /// <summary>
        /// Gets remote stock for single product
        /// </summary>
        /// <param name="product">Product to query</param>
        private static void Get(string product)
        {
            Get(new string[] { product });
        }

        /// <summary>
        /// Exits menu
        /// </summary>
        private static void Back()
        {
            exitToken = true;
        }
    }
}
