﻿// <copyright file="Editor.cs" company="Apple Inc.">
// All rights reserved. © 2018
// </copyright>

namespace AppleStore.Program
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using AppleStore.Logic;
    using Menu;

    /// <summary>
    /// Editor for entity
    /// </summary>
    /// <typeparam name="TEntity">Type of entity</typeparam>
    internal class Editor<TEntity>
        where TEntity : class, new()
    {
        /// <summary>
        /// Exit token for editor menu
        /// </summary>
        private bool exitToken = false;

        /// <summary>
        /// Exit token for single prop editor
        /// </summary>
        private bool editExitToken = false;

        /// <summary>
        /// Menu class
        /// </summary>
        private MenuClass menu;

        /// <summary>
        /// Editing entity
        /// </summary>
        private TEntity entity;

        /// <summary>
        /// TEntity's logic object
        /// </summary>
        private ILogic<TEntity> logic;

        /// <summary>
        /// Translator for displaying strings
        /// </summary>
        private Translator translator;

        /// <summary>
        /// Active property for saving modifications
        /// </summary>
        private PropertyInfo activeproperty;

        /// <summary>
        /// Determines if entity is new
        /// </summary>
        private bool create;

        /// <summary>
        /// Properties section for menu (for reloads)
        /// </summary>
        private MenuSection properties;

        /// <summary>
        /// Initializes a new instance of the <see cref="Editor{TEntity}"/> class.
        /// </summary>
        /// <param name="logic">Logic class</param>
        /// <param name="entity">Entity to be edited</param>
        /// <param name="t">Translator for displaying text</param>
        public Editor(ILogic<TEntity> logic, TEntity entity, Translator t)
        {
            this.entity = entity;
            this.translator = t;
            this.logic = logic;

            this.MakeMenu();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Editor{TEntity}"/> class.
        /// Sets it up for creating a new entity.
        /// </summary>
        /// <param name="logic">Logic class</param>
        /// <param name="t">Translator for displaying text</param>
        public Editor(ILogic<TEntity> logic, Translator t)
        {
            this.create = true;
            this.entity = new TEntity();
            this.logic = logic;
            this.translator = t;

            this.MakeMenu();
        }

        /// <summary>
        /// Constructs the editor menu class
        /// </summary>
        public void MakeMenu()
        {
            IMenuItem[] interactions = new IMenuItem[]
            {
                new MenuAction("Mentés", this.Save),
                new MenuAction("Törlés", this.Delete),
                new MenuAction("Vissza", this.Back)
            };

            this.properties = new MenuSection("Mezők", this.MakePropertyItems());
            MenuSection[] sections = new MenuSection[]
            {
                this.properties,
                new MenuSection("Interakció", interactions)
            };
            this.menu = new MenuClass(sections,  "Szerkesztés");
        }

        /// <summary>
        /// Constructs property menu items
        /// </summary>
        /// <returns>Property items</returns>
        public IMenuItem[] MakePropertyItems()
        {
            IMenuItem[] properties = new IMenuItem[this.logic.Properties.Length];
            int index = 0;
            foreach (string property in this.logic.Properties)
            {
                PropertyInfo prop = typeof(TEntity).GetProperty(property);
                MethodInfo getter = this.logic.GetType().GetMethod("Get" + property);
                object value = prop.GetValue(this.entity);
                if (getter != null)
                {
                    value = getter.Invoke(this.logic, new object[] { value });
                }

                string title = string.Format("{0}: {1}", this.translator.Translate(property), value == null ? "-" : value.ToString());
                properties[index++] = new MenuFunction<PropertyInfo>(title, this.EditProperty, prop);
            }

            return properties;
        }

        /// <summary>
        /// Edits a single property
        /// </summary>
        /// <param name="prop">Property to be modified</param>
        public void EditProperty(PropertyInfo prop)
        {
            this.activeproperty = prop;
            this.editExitToken = false;
            MethodInfo options = this.logic.GetType().GetMethod("Options" + prop.Name);
            if (options != null)
            {
                IMenuItem[] chooseitems = default(IMenuItem[]);
                int index = 0;
                object results = options.Invoke(this.logic, null);

                if (results.GetType() == typeof(Dictionary<object, object>))
                {
                    Dictionary<object, object> dict = (Dictionary<object, object>)results;
                    chooseitems = new IMenuItem[dict.Count];
                    foreach (KeyValuePair<object, object> kvp in dict)
                    {
                        chooseitems[index++] = new MenuFunction<object>(kvp.Value.ToString(), this.SetActiveProperty, kvp.Key);
                    }
                }
                else if (results.GetType() == typeof(string[]))
                {
                    string[] array = (string[])results;
                    chooseitems = new IMenuItem[array.Length];
                    for (int i = 0; i < array.Length; i++)
                    {
                        chooseitems[i] = new MenuFunction<object>(array[i], this.SetActiveProperty, array[i]);
                    }
                }

                IMenuItem[] interactionitems = new IMenuItem[]
                {
                    new MenuAction("Vissza", this.BackEdit)
                };

                MenuSection choose = new MenuSection(string.Empty, chooseitems);
                MenuSection interaction = new MenuSection("Interakció", interactionitems);

                MenuClass menu = new MenuClass(new MenuSection[] { choose, interaction }, this.translator.Translate(prop.Name));
                menu.Start(ref this.editExitToken);
            }
            else
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.White;
                Console.BackgroundColor = ConsoleColor.DarkBlue;
                Console.WriteLine(this.translator.Translate(prop.Name));
                Console.ResetColor();
                string line = Console.ReadLine();

                this.SetActiveProperty(line);
            }

            // Reload items
            this.properties.Reload(this.MakePropertyItems());
        }

        /// <summary>
        /// Sets value to active property
        /// </summary>
        /// <param name="value">Value to be set</param>
        public void SetActiveProperty(object value)
        {
            this.BackEdit();

            try
            {
                this.activeproperty.SetValue(this.entity, value);
            }
            catch (ArgumentException)
            {
                string val = (string)value;
                if (val != string.Empty)
                {
                    Type t = this.activeproperty.PropertyType;
                    if (t == typeof(int?))
                    {
                        this.activeproperty.SetValue(this.entity, int.Parse(val));
                    }
                    else if (t == typeof(DateTime?))
                    {
                        this.activeproperty.SetValue(this.entity, DateTime.Parse(val));
                    }
                }
            }
        }

        /// <summary>
        /// Starts the editor
        /// </summary>
        public void Start()
        {
            this.exitToken = false;
            this.menu.Start(ref this.exitToken);
        }

        /// <summary>
        /// Saves entity and returns to table view
        /// </summary>
        public void Save()
        {
            if (this.create)
            {
                this.logic.Add(this.entity);
            }
            else
            {
                this.logic.Save(this.entity);
            }

            this.Back();
        }

        /// <summary>
        /// Deletes entity and returns to table view
        /// </summary>
        public void Delete()
        {
            this.logic.Delete(this.entity);
            this.Back();
        }

        /// <summary>
        /// Returns to table view
        /// </summary>
        private void Back()
        {
            this.exitToken = true;
        }

        /// <summary>
        /// Returns to editor
        /// </summary>
        private void BackEdit()
        {
            this.editExitToken = true;
        }
    }
}
