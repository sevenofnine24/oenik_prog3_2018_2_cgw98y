﻿// <copyright file="IRepository.cs" company="Apple Inc.">
// All rights reserved. © 2018
// </copyright>

namespace AppleStore.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Repository interface
    /// </summary>
    /// <typeparam name="TEntity">Entity type</typeparam>
    public interface IRepository<TEntity> : IDisposable
        where TEntity : class
    {
        /// <summary>
        /// All records
        /// </summary>
        /// <returns>All entities</returns>
        IQueryable<TEntity> All();

        /// <summary>
        /// Where selector
        /// </summary>
        /// <param name="selector">Selector method</param>
        /// <returns>Enumerable entities</returns>
        IEnumerable<TEntity> Where(Func<TEntity, bool> selector);

        /// <summary>
        /// Creates a record
        /// </summary>
        /// <param name="entity">Entity to be added</param>
        void Add(TEntity entity);

        /// <summary>
        /// Updates a record
        /// </summary>
        /// <param name="entity">Entity to be saved</param>
        void Save(TEntity entity);

        /// <summary>
        /// Deletes a record
        /// </summary>
        /// <param name="entity">Entity to be deleted</param>
        void Delete(TEntity entity);
    }
}
