﻿// <copyright file="SqlRepository.cs" company="Apple Inc.">
// All rights reserved. © 2018
// </copyright>

namespace AppleStore.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using AppleStore.Data;

    /// <summary>
    /// SQL repository
    /// Implements IRepository
    /// </summary>
    /// <typeparam name="TEntity">Type of entity</typeparam>
    public sealed class SqlRepository<TEntity> : IRepository<TEntity>
        where TEntity : class, new()
    {
        /// <summary>
        /// Database context
        /// </summary>
        private DatabaseEntities ctx;

        /// <summary>
        /// Entities of TEntity type
        /// </summary>
        private DbSet<TEntity> entities;

        /// <summary>
        /// Initializes a new instance of the <see cref="SqlRepository{TEntity}"/> class.
        /// </summary>
        public SqlRepository()
        {
            this.ctx = new DatabaseEntities();
            this.entities = this.ctx.Set<TEntity>();
        }

        /// <inheritdoc />
        public void Add(TEntity entity)
        {
            this.entities.Add(entity);
            this.Save();
        }

        /// <inheritdoc />
        public void Save(TEntity entity = null)
        {
            this.ctx.SaveChanges();
        }

        /// <inheritdoc />
        public IQueryable<TEntity> All()
        {
            return this.entities;
        }

        /// <inheritdoc />
        public IEnumerable<TEntity> Where(Func<TEntity, bool> selector)
        {
            return this.entities.Where(selector);
        }

        /// <inheritdoc />
        public void Delete(TEntity entity)
        {
            this.entities.Remove(entity);
            this.Save();
        }

        /// <summary>
        /// Disposes of context field
        /// </summary>
        public void Dispose()
        {
            this.ctx.Dispose();
        }
    }
}
