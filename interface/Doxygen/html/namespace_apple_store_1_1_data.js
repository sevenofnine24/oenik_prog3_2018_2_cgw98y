var namespace_apple_store_1_1_data =
[
    [ "Colors", "class_apple_store_1_1_data_1_1_colors.html", "class_apple_store_1_1_data_1_1_colors" ],
    [ "Customers", "class_apple_store_1_1_data_1_1_customers.html", "class_apple_store_1_1_data_1_1_customers" ],
    [ "DatabaseEntities", "class_apple_store_1_1_data_1_1_database_entities.html", "class_apple_store_1_1_data_1_1_database_entities" ],
    [ "Orders", "class_apple_store_1_1_data_1_1_orders.html", "class_apple_store_1_1_data_1_1_orders" ],
    [ "ProductCategory", "class_apple_store_1_1_data_1_1_product_category.html", "class_apple_store_1_1_data_1_1_product_category" ],
    [ "Products", "class_apple_store_1_1_data_1_1_products.html", "class_apple_store_1_1_data_1_1_products" ],
    [ "Stock", "class_apple_store_1_1_data_1_1_stock.html", "class_apple_store_1_1_data_1_1_stock" ],
    [ "StoragePrice", "class_apple_store_1_1_data_1_1_storage_price.html", "class_apple_store_1_1_data_1_1_storage_price" ]
];