var class_apple_store_1_1_data_1_1_database_entities =
[
    [ "DatabaseEntities", "class_apple_store_1_1_data_1_1_database_entities.html#a13778f7b92d81331fc9592fd8e96c0fa", null ],
    [ "OnModelCreating", "class_apple_store_1_1_data_1_1_database_entities.html#a22971d7691c33525ccbf3bee8552fc5e", null ],
    [ "Colors", "class_apple_store_1_1_data_1_1_database_entities.html#ae2e2f5a8d2a903debc0e3276bbad22a2", null ],
    [ "Customers", "class_apple_store_1_1_data_1_1_database_entities.html#aba9f7d7731024901bf383ff715a359f7", null ],
    [ "Orders", "class_apple_store_1_1_data_1_1_database_entities.html#a8aba0620da5607c67424b81b4856f90e", null ],
    [ "ProductCategory", "class_apple_store_1_1_data_1_1_database_entities.html#af50d4a11f410c712cb275991717f62ec", null ],
    [ "Products", "class_apple_store_1_1_data_1_1_database_entities.html#accfb187cc1b602e62bd19c3343f0126e", null ],
    [ "Stock", "class_apple_store_1_1_data_1_1_database_entities.html#ab3ccad62e286be772bf2afb1896a6aec", null ],
    [ "StoragePrice", "class_apple_store_1_1_data_1_1_database_entities.html#a54202099580385a0352751b909da13b1", null ]
];