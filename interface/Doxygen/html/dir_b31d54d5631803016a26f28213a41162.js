var dir_b31d54d5631803016a26f28213a41162 =
[
    [ "AppleStore.Data", "dir_f0a8be2aff79cfc32687794789b920f8.html", "dir_f0a8be2aff79cfc32687794789b920f8" ],
    [ "AppleStore.Logic", "dir_d4a6981fe07ca55e1d77202035363be6.html", "dir_d4a6981fe07ca55e1d77202035363be6" ],
    [ "AppleStore.Logic.Tests", "dir_6b71f7ac8e3131bbef0d4de816731c90.html", "dir_6b71f7ac8e3131bbef0d4de816731c90" ],
    [ "AppleStore.Menu", "dir_7d438757a250ccd0aca6e1f89b317ad0.html", "dir_7d438757a250ccd0aca6e1f89b317ad0" ],
    [ "AppleStore.Program", "dir_e01e346b774e296dfa93885a227a8c5d.html", "dir_e01e346b774e296dfa93885a227a8c5d" ],
    [ "AppleStore.Repository", "dir_36f54d6540b480fe00e3c18e8d7474c8.html", "dir_36f54d6540b480fe00e3c18e8d7474c8" ],
    [ "AppleStore.Repository.Tests", "dir_a3461d8215266591b03909e73e230ac6.html", "dir_a3461d8215266591b03909e73e230ac6" ]
];