var class_apple_store_1_1_logic_1_1_stock_logic =
[
    [ "StockLogic", "class_apple_store_1_1_logic_1_1_stock_logic.html#a51c6e6ec33a2f23dcc50b165a381de0d", null ],
    [ "Dispose", "class_apple_store_1_1_logic_1_1_stock_logic.html#ad30aa6ea923dd32a290d562da6038748", null ],
    [ "Formatted", "class_apple_store_1_1_logic_1_1_stock_logic.html#a1fa284da2e28c7753b6720c40e62c238", null ],
    [ "GetColor", "class_apple_store_1_1_logic_1_1_stock_logic.html#a1adca393e45161ba97f063d92f23bd55", null ],
    [ "GetProduct", "class_apple_store_1_1_logic_1_1_stock_logic.html#a9102ef2d684c2858599d12ff02bb6a0f", null ],
    [ "OptionsColor", "class_apple_store_1_1_logic_1_1_stock_logic.html#a608a1404ab923f2b4d3110ad00e8ef2a", null ],
    [ "OptionsProduct", "class_apple_store_1_1_logic_1_1_stock_logic.html#a1de7ba424b6c7ea389e4fe7eccab4fe5", null ],
    [ "StockPrice", "class_apple_store_1_1_logic_1_1_stock_logic.html#ae3b53ac85368b20d7676d35e4bc214f8", null ],
    [ "Headings", "class_apple_store_1_1_logic_1_1_stock_logic.html#a0c7bdf0067326c8a95ff9affd60ff213", null ],
    [ "Properties", "class_apple_store_1_1_logic_1_1_stock_logic.html#a874d33d0e2b5f9869cd3cf85f9ec3d82", null ],
    [ "Title", "class_apple_store_1_1_logic_1_1_stock_logic.html#a41a9d3cfde344c91433db43bcc846867", null ]
];