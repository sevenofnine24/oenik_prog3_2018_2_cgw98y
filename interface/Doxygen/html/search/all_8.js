var searchData=
[
  ['ilogic',['ILogic',['../interface_apple_store_1_1_logic_1_1_i_logic.html',1,'AppleStore::Logic']]],
  ['ilogic_3c_20orders_20_3e',['ILogic&lt; Orders &gt;',['../interface_apple_store_1_1_logic_1_1_i_logic.html',1,'AppleStore::Logic']]],
  ['ilogic_3c_20products_20_3e',['ILogic&lt; Products &gt;',['../interface_apple_store_1_1_logic_1_1_i_logic.html',1,'AppleStore::Logic']]],
  ['ilogic_3c_20stock_20_3e',['ILogic&lt; Stock &gt;',['../interface_apple_store_1_1_logic_1_1_i_logic.html',1,'AppleStore::Logic']]],
  ['imenuitem',['IMenuItem',['../interface_apple_store_1_1_menu_1_1_i_menu_item.html',1,'AppleStore::Menu']]],
  ['invoke',['Invoke',['../interface_apple_store_1_1_menu_1_1_i_menu_item.html#a7984b16a511e0f8bf716c3763372cdc2',1,'AppleStore.Menu.IMenuItem.Invoke()'],['../class_apple_store_1_1_menu_1_1_menu_action.html#a0edd456709a3969d6bd683d488e61413',1,'AppleStore.Menu.MenuAction.Invoke()'],['../class_apple_store_1_1_menu_1_1_menu_choice.html#aebc2928a4249664927465a85c538d2e4',1,'AppleStore.Menu.MenuChoice.Invoke()'],['../class_apple_store_1_1_menu_1_1_menu_function.html#a30578f64037e558818fb5cff1eeeb714',1,'AppleStore.Menu.MenuFunction.Invoke()']]],
  ['irepository',['IRepository',['../interface_apple_store_1_1_repository_1_1_i_repository.html',1,'AppleStore::Repository']]],
  ['irepository_3c_20applestore_3a_3adata_3a_3acolors_20_3e',['IRepository&lt; AppleStore::Data::Colors &gt;',['../interface_apple_store_1_1_repository_1_1_i_repository.html',1,'AppleStore::Repository']]],
  ['irepository_3c_20applestore_3a_3adata_3a_3acustomers_20_3e',['IRepository&lt; AppleStore::Data::Customers &gt;',['../interface_apple_store_1_1_repository_1_1_i_repository.html',1,'AppleStore::Repository']]],
  ['irepository_3c_20applestore_3a_3adata_3a_3aproductcategory_20_3e',['IRepository&lt; AppleStore::Data::ProductCategory &gt;',['../interface_apple_store_1_1_repository_1_1_i_repository.html',1,'AppleStore::Repository']]],
  ['irepository_3c_20applestore_3a_3adata_3a_3aproducts_20_3e',['IRepository&lt; AppleStore::Data::Products &gt;',['../interface_apple_store_1_1_repository_1_1_i_repository.html',1,'AppleStore::Repository']]]
];
