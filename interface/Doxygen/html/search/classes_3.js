var searchData=
[
  ['ilogic',['ILogic',['../interface_apple_store_1_1_logic_1_1_i_logic.html',1,'AppleStore::Logic']]],
  ['ilogic_3c_20orders_20_3e',['ILogic&lt; Orders &gt;',['../interface_apple_store_1_1_logic_1_1_i_logic.html',1,'AppleStore::Logic']]],
  ['ilogic_3c_20products_20_3e',['ILogic&lt; Products &gt;',['../interface_apple_store_1_1_logic_1_1_i_logic.html',1,'AppleStore::Logic']]],
  ['ilogic_3c_20stock_20_3e',['ILogic&lt; Stock &gt;',['../interface_apple_store_1_1_logic_1_1_i_logic.html',1,'AppleStore::Logic']]],
  ['imenuitem',['IMenuItem',['../interface_apple_store_1_1_menu_1_1_i_menu_item.html',1,'AppleStore::Menu']]],
  ['irepository',['IRepository',['../interface_apple_store_1_1_repository_1_1_i_repository.html',1,'AppleStore::Repository']]],
  ['irepository_3c_20applestore_3a_3adata_3a_3acolors_20_3e',['IRepository&lt; AppleStore::Data::Colors &gt;',['../interface_apple_store_1_1_repository_1_1_i_repository.html',1,'AppleStore::Repository']]],
  ['irepository_3c_20applestore_3a_3adata_3a_3acustomers_20_3e',['IRepository&lt; AppleStore::Data::Customers &gt;',['../interface_apple_store_1_1_repository_1_1_i_repository.html',1,'AppleStore::Repository']]],
  ['irepository_3c_20applestore_3a_3adata_3a_3aproductcategory_20_3e',['IRepository&lt; AppleStore::Data::ProductCategory &gt;',['../interface_apple_store_1_1_repository_1_1_i_repository.html',1,'AppleStore::Repository']]],
  ['irepository_3c_20applestore_3a_3adata_3a_3aproducts_20_3e',['IRepository&lt; AppleStore::Data::Products &gt;',['../interface_apple_store_1_1_repository_1_1_i_repository.html',1,'AppleStore::Repository']]]
];
