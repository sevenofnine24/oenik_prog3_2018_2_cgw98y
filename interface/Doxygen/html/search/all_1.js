var searchData=
[
  ['back',['Back',['../class_apple_store_1_1_program_1_1_table_view.html#a958ea420707d9eae88cda46aede2cb2d',1,'AppleStore::Program::TableView']]],
  ['baselogic',['BaseLogic',['../class_apple_store_1_1_logic_1_1_base_logic.html',1,'AppleStore.Logic.BaseLogic&lt; TEntity &gt;'],['../class_apple_store_1_1_logic_1_1_base_logic.html#acfcc671116349e259c8600ebefa5dbfb',1,'AppleStore.Logic.BaseLogic.BaseLogic()']]],
  ['baselogic_3c_20orders_20_3e',['BaseLogic&lt; Orders &gt;',['../class_apple_store_1_1_logic_1_1_base_logic.html',1,'AppleStore::Logic']]],
  ['baselogic_3c_20products_20_3e',['BaseLogic&lt; Products &gt;',['../class_apple_store_1_1_logic_1_1_base_logic.html',1,'AppleStore::Logic']]],
  ['baselogic_3c_20stock_20_3e',['BaseLogic&lt; Stock &gt;',['../class_apple_store_1_1_logic_1_1_base_logic.html',1,'AppleStore::Logic']]],
  ['baseprice',['BasePrice',['../class_apple_store_1_1_logic_1_1_stock_price_helper.html#a09ea353d2e0f76a6639092479c30b9be',1,'AppleStore::Logic::StockPriceHelper']]]
];
