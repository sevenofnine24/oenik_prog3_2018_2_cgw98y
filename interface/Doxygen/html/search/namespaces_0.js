var searchData=
[
  ['applestore',['AppleStore',['../namespace_apple_store.html',1,'']]],
  ['data',['Data',['../namespace_apple_store_1_1_data.html',1,'AppleStore']]],
  ['logic',['Logic',['../namespace_apple_store_1_1_logic.html',1,'AppleStore']]],
  ['menu',['Menu',['../namespace_apple_store_1_1_menu.html',1,'AppleStore']]],
  ['program',['Program',['../namespace_apple_store_1_1_program.html',1,'AppleStore']]],
  ['repository',['Repository',['../namespace_apple_store_1_1_repository.html',1,'AppleStore']]],
  ['tests',['Tests',['../namespace_apple_store_1_1_logic_1_1_tests.html',1,'AppleStore.Logic.Tests'],['../namespace_apple_store_1_1_repository_1_1_tests.html',1,'AppleStore.Repository.Tests']]]
];
