var searchData=
[
  ['optionscategory',['OptionsCategory',['../class_apple_store_1_1_logic_1_1_products_logic.html#a85e74bbf2079c15ed2dfdaec772e6d9c',1,'AppleStore::Logic::ProductsLogic']]],
  ['optionscolor',['OptionsColor',['../class_apple_store_1_1_logic_1_1_stock_logic.html#a608a1404ab923f2b4d3110ad00e8ef2a',1,'AppleStore::Logic::StockLogic']]],
  ['optionscustomer',['OptionsCustomer',['../class_apple_store_1_1_logic_1_1_orders_logic.html#a629b377abc546650225a6799345bdc83',1,'AppleStore::Logic::OrdersLogic']]],
  ['optionsproduct',['OptionsProduct',['../class_apple_store_1_1_logic_1_1_stock_logic.html#a1de7ba424b6c7ea389e4fe7eccab4fe5',1,'AppleStore::Logic::StockLogic']]],
  ['orders',['Orders',['../class_apple_store_1_1_data_1_1_orders.html',1,'AppleStore::Data']]],
  ['orderslogic',['OrdersLogic',['../class_apple_store_1_1_logic_1_1_orders_logic.html',1,'AppleStore.Logic.OrdersLogic'],['../class_apple_store_1_1_logic_1_1_orders_logic.html#a03cea5ca500eb127efa05228000e9cda',1,'AppleStore.Logic.OrdersLogic.OrdersLogic()']]]
];
