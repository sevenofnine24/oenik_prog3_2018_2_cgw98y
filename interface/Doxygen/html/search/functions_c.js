var searchData=
[
  ['save',['Save',['../class_apple_store_1_1_logic_1_1_base_logic.html#a316d3721a083a55e474ebfa07e639535',1,'AppleStore.Logic.BaseLogic.Save()'],['../interface_apple_store_1_1_logic_1_1_i_logic.html#ae2ac9c7603b84cc5241e415467fd170e',1,'AppleStore.Logic.ILogic.Save()'],['../interface_apple_store_1_1_repository_1_1_i_repository.html#a4dd6a68bf1e1ed4e8081d857add60397',1,'AppleStore.Repository.IRepository.Save()'],['../class_apple_store_1_1_repository_1_1_sql_repository.html#ad2ae406cd65035731d33fec43bb24813',1,'AppleStore.Repository.SqlRepository.Save()']]],
  ['sqlrepository',['SqlRepository',['../class_apple_store_1_1_repository_1_1_sql_repository.html#a46ddb14e56a9bf98b847f9be61c343d2',1,'AppleStore::Repository::SqlRepository']]],
  ['start',['Start',['../class_apple_store_1_1_menu_1_1_menu_class.html#aa63db96c9b6e1273d9ccc3431205cb07',1,'AppleStore.Menu.MenuClass.Start()'],['../class_apple_store_1_1_program_1_1_table_view.html#a70fc5df129d39b34127a203ec6e12726',1,'AppleStore.Program.TableView.Start()']]],
  ['stocklogic',['StockLogic',['../class_apple_store_1_1_logic_1_1_stock_logic.html#a51c6e6ec33a2f23dcc50b165a381de0d',1,'AppleStore::Logic::StockLogic']]],
  ['stockprice',['StockPrice',['../class_apple_store_1_1_logic_1_1_stock_logic.html#ae3b53ac85368b20d7676d35e4bc214f8',1,'AppleStore::Logic::StockLogic']]],
  ['sumpercustomer',['SumPerCustomer',['../class_apple_store_1_1_logic_1_1_orders_logic.html#a6fffdd5532aa998b9af473b1b228306b',1,'AppleStore::Logic::OrdersLogic']]]
];
