var searchData=
[
  ['action',['Action',['../class_apple_store_1_1_menu_1_1_menu_action.html#ab136964ba5f60121ab8c53cf6d3337cf',1,'AppleStore.Menu.MenuAction.Action()'],['../class_apple_store_1_1_menu_1_1_menu_function.html#a7189969b20e627d9a15c5f61f9075290',1,'AppleStore.Menu.MenuFunction.Action()']]],
  ['activesection',['ActiveSection',['../class_apple_store_1_1_menu_1_1_menu_class.html#ad232da63d99594706704371feeb0f495',1,'AppleStore::Menu::MenuClass']]],
  ['add',['Add',['../class_apple_store_1_1_logic_1_1_base_logic.html#ae49dfae0199b1b7c4e7219845b3dbd92',1,'AppleStore.Logic.BaseLogic.Add()'],['../interface_apple_store_1_1_logic_1_1_i_logic.html#a90ff8f679b74b05bfd78768d83738b83',1,'AppleStore.Logic.ILogic.Add()'],['../interface_apple_store_1_1_repository_1_1_i_repository.html#ae23cac0937163c977969c450e11be48e',1,'AppleStore.Repository.IRepository.Add()'],['../class_apple_store_1_1_repository_1_1_sql_repository.html#ac029c202aa441ff4246523cb7295957b',1,'AppleStore.Repository.SqlRepository.Add()']]],
  ['all',['All',['../class_apple_store_1_1_logic_1_1_base_logic.html#ad03c9d81350d21acc6289fe2b60a8a28',1,'AppleStore.Logic.BaseLogic.All()'],['../interface_apple_store_1_1_logic_1_1_i_logic.html#aad40e8f919a350d61643db50992fed07',1,'AppleStore.Logic.ILogic.All()'],['../interface_apple_store_1_1_repository_1_1_i_repository.html#a4b66092df009455f61df8fe29fcd657c',1,'AppleStore.Repository.IRepository.All()'],['../class_apple_store_1_1_repository_1_1_sql_repository.html#ad16cca2479852ed4af6dd9676197edd1',1,'AppleStore.Repository.SqlRepository.All()']]],
  ['applestore',['AppleStore',['../namespace_apple_store.html',1,'']]],
  ['argument',['Argument',['../class_apple_store_1_1_menu_1_1_menu_function.html#ad5f9d1534d425e792608f5b20b7cc20c',1,'AppleStore::Menu::MenuFunction']]],
  ['averagepricepercategory',['AveragePricePerCategory',['../class_apple_store_1_1_logic_1_1_products_logic.html#a8c9222dbe31fb7a73cb34f1d1116ae04',1,'AppleStore::Logic::ProductsLogic']]],
  ['data',['Data',['../namespace_apple_store_1_1_data.html',1,'AppleStore']]],
  ['logic',['Logic',['../namespace_apple_store_1_1_logic.html',1,'AppleStore']]],
  ['menu',['Menu',['../namespace_apple_store_1_1_menu.html',1,'AppleStore']]],
  ['program',['Program',['../namespace_apple_store_1_1_program.html',1,'AppleStore']]],
  ['repository',['Repository',['../namespace_apple_store_1_1_repository.html',1,'AppleStore']]],
  ['tests',['Tests',['../namespace_apple_store_1_1_logic_1_1_tests.html',1,'AppleStore.Logic.Tests'],['../namespace_apple_store_1_1_repository_1_1_tests.html',1,'AppleStore.Repository.Tests']]]
];
