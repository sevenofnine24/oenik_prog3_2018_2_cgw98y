var class_apple_store_1_1_data_1_1_products =
[
    [ "Products", "class_apple_store_1_1_data_1_1_products.html#a802f7695941467eb12a672b9c8130413", null ],
    [ "Category", "class_apple_store_1_1_data_1_1_products.html#a46372cea9ec325b9ace438d7a763e9b3", null ],
    [ "Description", "class_apple_store_1_1_data_1_1_products.html#a1a807e7201aef97ffa14e2a8f8715cee", null ],
    [ "Id", "class_apple_store_1_1_data_1_1_products.html#a0158d399a0f35efc1ecf9532a61dd6f8", null ],
    [ "Name", "class_apple_store_1_1_data_1_1_products.html#ad544692250e54feb48f4eef7d1069ba1", null ],
    [ "Price", "class_apple_store_1_1_data_1_1_products.html#a136e9c3486933c6b296a1c9a2e1ecef5", null ],
    [ "ProductCategory", "class_apple_store_1_1_data_1_1_products.html#a1ff5c53e2372db145c6a06cedd1e1446", null ],
    [ "ReleaseDate", "class_apple_store_1_1_data_1_1_products.html#ac4ba4df5c5cd8f8b07f3ad52b890edab", null ],
    [ "Stock", "class_apple_store_1_1_data_1_1_products.html#a28582863751e36546d7687af41885bf4", null ]
];