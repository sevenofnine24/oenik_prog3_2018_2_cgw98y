var namespace_apple_store =
[
    [ "Data", "namespace_apple_store_1_1_data.html", "namespace_apple_store_1_1_data" ],
    [ "Logic", "namespace_apple_store_1_1_logic.html", "namespace_apple_store_1_1_logic" ],
    [ "Menu", "namespace_apple_store_1_1_menu.html", "namespace_apple_store_1_1_menu" ],
    [ "Program", "namespace_apple_store_1_1_program.html", "namespace_apple_store_1_1_program" ],
    [ "Repository", "namespace_apple_store_1_1_repository.html", "namespace_apple_store_1_1_repository" ]
];