var interface_apple_store_1_1_logic_1_1_i_logic =
[
    [ "Add", "interface_apple_store_1_1_logic_1_1_i_logic.html#a90ff8f679b74b05bfd78768d83738b83", null ],
    [ "All", "interface_apple_store_1_1_logic_1_1_i_logic.html#aad40e8f919a350d61643db50992fed07", null ],
    [ "Delete", "interface_apple_store_1_1_logic_1_1_i_logic.html#a5416acd48b3f540c91144f3d441e48e7", null ],
    [ "Formatted", "interface_apple_store_1_1_logic_1_1_i_logic.html#aea9c57c3629c9edbe6a1dff2a78acce8", null ],
    [ "Save", "interface_apple_store_1_1_logic_1_1_i_logic.html#ae2ac9c7603b84cc5241e415467fd170e", null ],
    [ "Headings", "interface_apple_store_1_1_logic_1_1_i_logic.html#a8b4e0435307f9307a6e3e8918b73fa50", null ],
    [ "Properties", "interface_apple_store_1_1_logic_1_1_i_logic.html#acb2d03b95dd5844a2c9ab39af4db2e85", null ],
    [ "Title", "interface_apple_store_1_1_logic_1_1_i_logic.html#aa0091c155df54e966fd34f397ca526ea", null ]
];