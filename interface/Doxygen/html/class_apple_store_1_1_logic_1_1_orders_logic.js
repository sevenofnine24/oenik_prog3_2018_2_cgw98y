var class_apple_store_1_1_logic_1_1_orders_logic =
[
    [ "OrdersLogic", "class_apple_store_1_1_logic_1_1_orders_logic.html#a03cea5ca500eb127efa05228000e9cda", null ],
    [ "Dispose", "class_apple_store_1_1_logic_1_1_orders_logic.html#ae7ba07affcb8ad7b00ad29f183969494", null ],
    [ "Formatted", "class_apple_store_1_1_logic_1_1_orders_logic.html#a598fb030433ab3428d4864491a7326d2", null ],
    [ "GetCustomer", "class_apple_store_1_1_logic_1_1_orders_logic.html#a6dd60c603de048f2305dc4ea78d00528", null ],
    [ "OptionsCustomer", "class_apple_store_1_1_logic_1_1_orders_logic.html#a629b377abc546650225a6799345bdc83", null ],
    [ "SumPerCustomer", "class_apple_store_1_1_logic_1_1_orders_logic.html#a6fffdd5532aa998b9af473b1b228306b", null ],
    [ "Headings", "class_apple_store_1_1_logic_1_1_orders_logic.html#a7925af84ab3d8d040d5730fa82fd1107", null ],
    [ "Properties", "class_apple_store_1_1_logic_1_1_orders_logic.html#a17c613de6e5d208b7825d033460ebde6", null ],
    [ "Title", "class_apple_store_1_1_logic_1_1_orders_logic.html#aab507f6ce4ed81980f92000cd7ae0a03", null ]
];