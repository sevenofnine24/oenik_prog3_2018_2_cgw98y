var class_apple_store_1_1_repository_1_1_sql_repository =
[
    [ "SqlRepository", "class_apple_store_1_1_repository_1_1_sql_repository.html#a46ddb14e56a9bf98b847f9be61c343d2", null ],
    [ "Add", "class_apple_store_1_1_repository_1_1_sql_repository.html#ac029c202aa441ff4246523cb7295957b", null ],
    [ "All", "class_apple_store_1_1_repository_1_1_sql_repository.html#ad16cca2479852ed4af6dd9676197edd1", null ],
    [ "Delete", "class_apple_store_1_1_repository_1_1_sql_repository.html#a3bc917fa5f8a71dc3d58f8d5adc09ad1", null ],
    [ "Dispose", "class_apple_store_1_1_repository_1_1_sql_repository.html#a9ce2a67e7ac88dd64f94da167295908d", null ],
    [ "Save", "class_apple_store_1_1_repository_1_1_sql_repository.html#ad2ae406cd65035731d33fec43bb24813", null ],
    [ "Where", "class_apple_store_1_1_repository_1_1_sql_repository.html#aae0382f185bc491d9c8d4d428e05f589", null ]
];