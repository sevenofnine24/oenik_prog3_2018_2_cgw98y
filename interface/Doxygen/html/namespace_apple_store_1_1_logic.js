var namespace_apple_store_1_1_logic =
[
    [ "BaseLogic", "class_apple_store_1_1_logic_1_1_base_logic.html", "class_apple_store_1_1_logic_1_1_base_logic" ],
    [ "ILogic", "interface_apple_store_1_1_logic_1_1_i_logic.html", "interface_apple_store_1_1_logic_1_1_i_logic" ],
    [ "OrdersLogic", "class_apple_store_1_1_logic_1_1_orders_logic.html", "class_apple_store_1_1_logic_1_1_orders_logic" ],
    [ "ProductsLogic", "class_apple_store_1_1_logic_1_1_products_logic.html", "class_apple_store_1_1_logic_1_1_products_logic" ],
    [ "StockLogic", "class_apple_store_1_1_logic_1_1_stock_logic.html", "class_apple_store_1_1_logic_1_1_stock_logic" ],
    [ "StockPriceHelper", "class_apple_store_1_1_logic_1_1_stock_price_helper.html", "class_apple_store_1_1_logic_1_1_stock_price_helper" ],
    [ "SumHelper", "class_apple_store_1_1_logic_1_1_sum_helper.html", "class_apple_store_1_1_logic_1_1_sum_helper" ]
];