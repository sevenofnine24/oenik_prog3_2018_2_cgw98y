var hierarchy =
[
    [ "AppleStore.Logic.BaseLogic< TEntity >", "class_apple_store_1_1_logic_1_1_base_logic.html", null ],
    [ "AppleStore.Logic.BaseLogic< Orders >", "class_apple_store_1_1_logic_1_1_base_logic.html", [
      [ "AppleStore.Logic.OrdersLogic", "class_apple_store_1_1_logic_1_1_orders_logic.html", null ]
    ] ],
    [ "AppleStore.Logic.BaseLogic< Products >", "class_apple_store_1_1_logic_1_1_base_logic.html", [
      [ "AppleStore.Logic.ProductsLogic", "class_apple_store_1_1_logic_1_1_products_logic.html", null ]
    ] ],
    [ "AppleStore.Logic.BaseLogic< Stock >", "class_apple_store_1_1_logic_1_1_base_logic.html", [
      [ "AppleStore.Logic.StockLogic", "class_apple_store_1_1_logic_1_1_stock_logic.html", null ]
    ] ],
    [ "AppleStore.Data.Colors", "class_apple_store_1_1_data_1_1_colors.html", null ],
    [ "AppleStore.Data.Customers", "class_apple_store_1_1_data_1_1_customers.html", null ],
    [ "DbContext", null, [
      [ "AppleStore.Data.DatabaseEntities", "class_apple_store_1_1_data_1_1_database_entities.html", null ]
    ] ],
    [ "IDisposable", null, [
      [ "AppleStore.Logic.ILogic< TEntity >", "interface_apple_store_1_1_logic_1_1_i_logic.html", null ],
      [ "AppleStore.Repository.IRepository< TEntity >", "interface_apple_store_1_1_repository_1_1_i_repository.html", [
        [ "AppleStore.Repository.SqlRepository< TEntity >", "class_apple_store_1_1_repository_1_1_sql_repository.html", null ]
      ] ]
    ] ],
    [ "AppleStore.Logic.ILogic< Orders >", "interface_apple_store_1_1_logic_1_1_i_logic.html", [
      [ "AppleStore.Logic.OrdersLogic", "class_apple_store_1_1_logic_1_1_orders_logic.html", null ]
    ] ],
    [ "AppleStore.Logic.ILogic< Products >", "interface_apple_store_1_1_logic_1_1_i_logic.html", [
      [ "AppleStore.Logic.ProductsLogic", "class_apple_store_1_1_logic_1_1_products_logic.html", null ]
    ] ],
    [ "AppleStore.Logic.ILogic< Stock >", "interface_apple_store_1_1_logic_1_1_i_logic.html", [
      [ "AppleStore.Logic.StockLogic", "class_apple_store_1_1_logic_1_1_stock_logic.html", null ]
    ] ],
    [ "AppleStore.Menu.IMenuItem", "interface_apple_store_1_1_menu_1_1_i_menu_item.html", [
      [ "AppleStore.Menu.MenuAction", "class_apple_store_1_1_menu_1_1_menu_action.html", null ],
      [ "AppleStore.Menu.MenuChoice", "class_apple_store_1_1_menu_1_1_menu_choice.html", null ],
      [ "AppleStore.Menu.MenuFunction< T >", "class_apple_store_1_1_menu_1_1_menu_function.html", null ]
    ] ],
    [ "AppleStore.Repository.IRepository< AppleStore.Data.Colors >", "interface_apple_store_1_1_repository_1_1_i_repository.html", null ],
    [ "AppleStore.Repository.IRepository< AppleStore.Data.Customers >", "interface_apple_store_1_1_repository_1_1_i_repository.html", null ],
    [ "AppleStore.Repository.IRepository< AppleStore.Data.ProductCategory >", "interface_apple_store_1_1_repository_1_1_i_repository.html", null ],
    [ "AppleStore.Repository.IRepository< AppleStore.Data.Products >", "interface_apple_store_1_1_repository_1_1_i_repository.html", null ],
    [ "AppleStore.Menu.MenuClass", "class_apple_store_1_1_menu_1_1_menu_class.html", null ],
    [ "AppleStore.Menu.MenuSection", "class_apple_store_1_1_menu_1_1_menu_section.html", null ],
    [ "AppleStore.Data.Orders", "class_apple_store_1_1_data_1_1_orders.html", null ],
    [ "AppleStore.Data.ProductCategory", "class_apple_store_1_1_data_1_1_product_category.html", null ],
    [ "AppleStore.Data.Products", "class_apple_store_1_1_data_1_1_products.html", null ],
    [ "AppleStore.Data.Stock", "class_apple_store_1_1_data_1_1_stock.html", null ],
    [ "AppleStore.Logic.StockPriceHelper", "class_apple_store_1_1_logic_1_1_stock_price_helper.html", null ],
    [ "AppleStore.Data.StoragePrice", "class_apple_store_1_1_data_1_1_storage_price.html", null ],
    [ "AppleStore.Logic.SumHelper", "class_apple_store_1_1_logic_1_1_sum_helper.html", null ],
    [ "AppleStore.Program.TableView< TEntity >", "class_apple_store_1_1_program_1_1_table_view.html", null ],
    [ "AppleStore.Program.Translator", "class_apple_store_1_1_program_1_1_translator.html", null ]
];