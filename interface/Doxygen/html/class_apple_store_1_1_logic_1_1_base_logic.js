var class_apple_store_1_1_logic_1_1_base_logic =
[
    [ "BaseLogic", "class_apple_store_1_1_logic_1_1_base_logic.html#acfcc671116349e259c8600ebefa5dbfb", null ],
    [ "Add", "class_apple_store_1_1_logic_1_1_base_logic.html#ae49dfae0199b1b7c4e7219845b3dbd92", null ],
    [ "All", "class_apple_store_1_1_logic_1_1_base_logic.html#ad03c9d81350d21acc6289fe2b60a8a28", null ],
    [ "Delete", "class_apple_store_1_1_logic_1_1_base_logic.html#aa72dfcf5a691d0b8d3364d7d7f046e01", null ],
    [ "Save", "class_apple_store_1_1_logic_1_1_base_logic.html#a316d3721a083a55e474ebfa07e639535", null ],
    [ "Repository", "class_apple_store_1_1_logic_1_1_base_logic.html#afc1eb77260c4ea34818f7e3f56559354", null ]
];