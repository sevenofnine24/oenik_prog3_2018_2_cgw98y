var class_apple_store_1_1_logic_1_1_products_logic =
[
    [ "ProductsLogic", "class_apple_store_1_1_logic_1_1_products_logic.html#a8f67947be052aab68b98779a5bd7e32c", null ],
    [ "AveragePricePerCategory", "class_apple_store_1_1_logic_1_1_products_logic.html#a8c9222dbe31fb7a73cb34f1d1116ae04", null ],
    [ "Dispose", "class_apple_store_1_1_logic_1_1_products_logic.html#aef68cdd86134cdda59b01954b3dd1c62", null ],
    [ "Formatted", "class_apple_store_1_1_logic_1_1_products_logic.html#a62fdda32f2dbb3b33fb747a25b9f9dfb", null ],
    [ "GetCategory", "class_apple_store_1_1_logic_1_1_products_logic.html#a3d46663c2a2970bdc1d5381df5f86c82", null ],
    [ "OptionsCategory", "class_apple_store_1_1_logic_1_1_products_logic.html#a85e74bbf2079c15ed2dfdaec772e6d9c", null ],
    [ "Headings", "class_apple_store_1_1_logic_1_1_products_logic.html#a20b6bba19cc3642a0effcbc3f384b42b", null ],
    [ "Properties", "class_apple_store_1_1_logic_1_1_products_logic.html#a8e91ca9d1d3fe14016330890497ca067", null ],
    [ "Title", "class_apple_store_1_1_logic_1_1_products_logic.html#a0585d29df6dc15e335f9593c8b9deae6", null ]
];