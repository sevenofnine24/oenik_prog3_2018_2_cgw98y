var class_apple_store_1_1_data_1_1_stock =
[
    [ "ArrivalDate", "class_apple_store_1_1_data_1_1_stock.html#a7b1f55ed7436f9781553342d05c2c8d0", null ],
    [ "Color", "class_apple_store_1_1_data_1_1_stock.html#a62f51f45760b19afc1cddc556b2d40c1", null ],
    [ "Colors", "class_apple_store_1_1_data_1_1_stock.html#ab14d4ef922c322a9af2404c4c47874fb", null ],
    [ "Id", "class_apple_store_1_1_data_1_1_stock.html#a206064f7066cff0e78a2b47cfd5daede", null ],
    [ "Product", "class_apple_store_1_1_data_1_1_stock.html#a49819a74f17fb7193b6afa39747a89dd", null ],
    [ "Products", "class_apple_store_1_1_data_1_1_stock.html#a7bef5158c7fc0a5f7822588f783e678e", null ],
    [ "Quantity", "class_apple_store_1_1_data_1_1_stock.html#a4a58c0a7bc14002f04efe75c5acb8ebb", null ],
    [ "Storage", "class_apple_store_1_1_data_1_1_stock.html#a19b5ed6674de5416dab2af05bdf67cd8", null ],
    [ "StoragePrice", "class_apple_store_1_1_data_1_1_stock.html#a2e24058c37979032804eab4e8bef98c4", null ]
];