﻿// <copyright file="Mocked.cs" company="Apple Inc.">
// All rights reserved. © 2018
// </copyright>

namespace AppleStore.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AppleStore.Data;
    using AppleStore.Repository;
    using Moq;

    /// <summary>
    /// Class for shorthand mocked switch table repositories
    /// </summary>
    internal class Mocked
    {
        /// <summary>
        /// Products repository
        /// </summary>
        /// <returns>Mocked repository</returns>
        public static Mock<IRepository<Products>> ProductsRepo()
        {
            Mock<IRepository<Products>> repo = new Mock<IRepository<Products>>();
            List<Products> items = new List<Products>()
            {
                new Products
                {
                    Id = 1,
                    Name = "Termék 1"
                },
                new Products
                {
                    Id = 2,
                    Name = "Termék 2"
                }
            };
            repo.Setup(x => x.All()).Returns(items.AsQueryable());

            return repo;
        }

        /// <summary>
        /// Colors repository
        /// </summary>
        /// <returns>Mocked repository</returns>
        public static Mock<IRepository<Colors>> ColorsRepo()
        {
            Mock<IRepository<Colors>> repo = new Mock<IRepository<Colors>>();
            List<Colors> items = new List<Colors>()
            {
                new Colors
                {
                    Id = 1,
                    Name = "Szín 1"
                },
                new Colors
                {
                    Id = 2,
                    Name = "Szín 2"
                }
            };
            repo.Setup(x => x.All()).Returns(items.AsQueryable());

            return repo;
        }

        /// <summary>
        /// Product category repository
        /// </summary>
        /// <returns>Mocked repository</returns>
        public static Mock<IRepository<ProductCategory>> CategoryRepo()
        {
            Mock<IRepository<ProductCategory>> repo = new Mock<IRepository<ProductCategory>>();
            List<ProductCategory> items = new List<ProductCategory>()
            {
                new ProductCategory
                {
                    Id = 1,
                    Name = "Kategória 1"
                },
                new ProductCategory
                {
                    Id = 2,
                    Name = "Kategória 2"
                }
            };
            repo.Setup(x => x.All()).Returns(items.AsQueryable());

            return repo;
        }

        /// <summary>
        /// Customers category repository
        /// </summary>
        /// <returns>Mocked repository</returns>
        public static Mock<IRepository<Customers>> CustomersRepo()
        {
            Mock<IRepository<Customers>> repo = new Mock<IRepository<Customers>>();
            List<Customers> items = new List<Customers>()
            {
                new Customers
                {
                    Id = 1,
                    Name = "Rendelő 1",
                    Address = "Cím 1"
                },
                new Customers
                {
                    Id = 2,
                    Name = "Rendelő 2",
                    Address = "Cím 2"
                }
            };
            repo.Setup(x => x.All()).Returns(items.AsQueryable());

            return repo;
        }
    }
}
