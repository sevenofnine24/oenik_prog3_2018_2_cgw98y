﻿// <copyright file="BaseLogicTests.cs" company="Apple Inc.">
// All rights reserved. © 2018
// </copyright>

namespace AppleStore.Logic.Tests
{
    using System.Collections.Generic;
    using System.Linq;
    using AppleStore.Data;
    using AppleStore.Logic;
    using AppleStore.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Test for base logic class
    /// </summary>
    [TestFixture]
    internal class BaseLogicTests
    {
        /// <summary>
        /// Storage for repo colors
        /// </summary>
        private List<Colors> colors;

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseLogicTests"/> class.
        /// Constructor for colors
        /// </summary>
        public BaseLogicTests()
        {
            this.colors = new List<Colors>()
            {
                new Colors { Id = 0, Name = "Piros" },
                new Colors { Id = 1, Name = "Kék" },
                new Colors { Id = 2, Name = "Zöld" },
                new Colors { Id = 3, Name = "Fekete" }
            };
        }

        /// <summary>
        /// Makes a mock repo with functionality
        /// </summary>
        /// <returns>Mock repository</returns>
        public Mock<IRepository<Colors>> MockRepoFactory()
        {
            Mock<IRepository<Colors>> repo = new Mock<IRepository<Colors>>(MockBehavior.Loose);
            repo.Setup(x => x.All()).Returns(this.colors.AsQueryable());
            repo.Setup(x => x.Add(It.IsAny<Colors>())).Callback((Colors color) => this.colors.Add(color));
            repo.Setup(x => x.Delete(It.IsAny<Colors>())).Callback((Colors color) => this.colors.Remove(color));

            return repo;
        }

        /// <summary>
        /// Makes a BaseLogic object
        /// </summary>
        /// <param name="repo">Repository for logic</param>
        /// <returns>BaseLogic object</returns>
        public BaseLogic<Colors> LogicFactory(Mock<IRepository<Colors>> repo)
        {
            return new BaseLogic<Colors>(repo.Object);
        }

        /// <summary>
        /// Tests all functionality
        /// </summary>
        [Test]
        public void All()
        {
            var repo = this.MockRepoFactory();
            var logic = this.LogicFactory(repo);

            var result = logic.All();
            Assert.That(result.Count, Is.EqualTo(this.colors.Count));
            Assert.That(result.Single(x => x.Id == 1).Name, Is.EqualTo("Kék"));

            repo.Verify(x => x.All(), Times.Once);
            repo.Verify(x => x.Add(It.IsAny<Colors>()), Times.Never);
        }

        /// <summary>
        /// Tests creating functionality
        /// </summary>
        [Test]
        public void Add()
        {
            var repo = this.MockRepoFactory();
            var logic = this.LogicFactory(repo);

            Colors newcolor = new Colors { Id = 4, Name = "Fehér" };
            int expected = this.colors.Count + 1;
            logic.Add(newcolor);
            Assert.That(logic.All().Count, Is.EqualTo(expected));
            Assert.That(logic.All().Single(x => x.Id == 4), Is.EqualTo(newcolor));

            repo.Verify(x => x.Save(It.IsAny<Colors>()), Times.Never);
            repo.Verify(x => x.Add(It.IsAny<Colors>()), Times.Once);
        }

        /// <summary>
        /// Test editing functionality
        /// </summary>
        [Test]
        public void Edit()
        {
            var repo = this.MockRepoFactory();
            var logic = this.LogicFactory(repo);

            Colors toedit = this.colors.Single(x => x.Id == 0);
            toedit.Name = "Lila";
            logic.Save(toedit);
            Assert.That(logic.All().Single(x => x.Id == 0).Name, Is.EqualTo("Lila"));

            repo.Verify(x => x.Save(It.IsAny<Colors>()), Times.Once);
        }

        /// <summary>
        /// Test deletion functionality
        /// </summary>
        [Test]
        public void Delete()
        {
            var repo = this.MockRepoFactory();
            var logic = this.LogicFactory(repo);

            Colors tobedeleted = this.colors.Single(x => x.Id == 2);
            int expected = this.colors.Count - 1;
            logic.Delete(tobedeleted);
            Assert.That(logic.All().Count, Is.EqualTo(expected));
            Assert.That(logic.All().Where(x => x.Id == 2).Count(), Is.EqualTo(0));

            repo.Verify(x => x.Delete(It.IsAny<Colors>()), Times.Once);
            repo.Verify(x => x.Save(It.IsAny<Colors>()), Times.Never);
        }
    }
}
