﻿// <copyright file="OrdersLogicTests.cs" company="Apple Inc.">
// All rights reserved. © 2018
// </copyright>

namespace AppleStore.Logic.Tests
{
    using System.Collections.Generic;
    using System.Linq;
    using AppleStore.Data;
    using AppleStore.Logic;
    using AppleStore.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Tests for Orders logic class
    /// </summary>
    [TestFixture]
    internal class OrdersLogicTests
    {
        /// <summary>
        /// Creates a mock repository
        /// </summary>
        /// <returns>Mocked repository</returns>
        public Mock<IRepository<Orders>> MockRepoFactory()
        {
            Mock<IRepository<Orders>> repo = new Mock<IRepository<Orders>>();
            List<Orders> orders = new List<Orders>()
            {
                new Orders()
                {
                    Customers = new Customers()
                    {
                        Name = "Név",
                        Address = "Cím"
                    },
                    PriceSum = 2500
                },
                new Orders()
                {
                    Customers = new Customers()
                    {
                        Name = "Név",
                        Address = "Cím"
                    },
                    PriceSum = 2500
                },
                new Orders()
                {
                    Customers = new Customers()
                    {
                        Name = "Másik",
                        Address = "Cím"
                    },
                    PriceSum = 4000
                }
            };
            repo.Setup(x => x.All()).Returns(orders.AsQueryable());

            return repo;
        }

        /// <summary>
        /// Test for sum per customer feature
        /// </summary>
        [Test]
        public void SumPerCustomer()
        {
            var repo = this.MockRepoFactory();
            var crepo = Mocked.CustomersRepo();

            using (OrdersLogic logic = new OrdersLogic(repo.Object, crepo.Object))
            {
                var result = logic.SumPerCustomer();

                Assert.That(result.Count(), Is.EqualTo(2));
                Assert.That(result.Single(x => x.CustomerName == "Név").Sum, Is.EqualTo(5000));
                Assert.That(result.Single(x => x.CustomerName == "Másik").Sum, Is.EqualTo(4000));

                repo.Verify(x => x.All(), Times.Once);
                repo.VerifyNoOtherCalls();
                crepo.VerifyNoOtherCalls();
            }
        }

        /// <summary>
        /// Tests customer translation
        /// </summary>
        [Test]
        public void GetCategory()
        {
            var repo = this.MockRepoFactory();
            var crepo = Mocked.CustomersRepo();

            using (OrdersLogic logic = new OrdersLogic(repo.Object, crepo.Object))
            {
                var result = logic.GetCustomer(2);

                Assert.That(result, Is.EqualTo("Rendelő 2 - Cím 2"));

                repo.VerifyNoOtherCalls();
                crepo.Verify(x => x.All(), Times.Once);
                crepo.VerifyNoOtherCalls();
            }
        }

        /// <summary>
        /// Tests customer option getter
        /// </summary>
        [Test]
        public void OptionsCategory()
        {
            var repo = this.MockRepoFactory();
            var crepo = Mocked.CustomersRepo();

            using (OrdersLogic logic = new OrdersLogic(repo.Object, crepo.Object))
            {
                var result = logic.OptionsCustomer();

                Assert.That(result.Length, Is.EqualTo(2));
                Assert.That(result[0], Is.EqualTo("Rendelő 1 - Cím 1"));

                repo.VerifyNoOtherCalls();
                crepo.Verify(x => x.All(), Times.Once);
                crepo.VerifyNoOtherCalls();
            }
        }
    }
}