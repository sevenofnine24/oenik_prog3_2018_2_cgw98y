﻿// <copyright file="ProductsLogicTests.cs" company="Apple Inc.">
// All rights reserved. © 2018
// </copyright>

namespace AppleStore.Logic.Tests
{
    using System.Collections.Generic;
    using System.Linq;
    using AppleStore.Data;
    using AppleStore.Logic;
    using AppleStore.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Tests for Products logic class
    /// </summary>
    [TestFixture]
    internal class ProductsLogicTests
    {
        /// <summary>
        /// Creates a mock repository
        /// </summary>
        /// <returns>Mocked repository</returns>
        public Mock<IRepository<Products>> MockRepoFactory()
        {
            Mock<IRepository<Products>> repo = new Mock<IRepository<Products>>();
            List<Products> products = new List<Products>()
            {
                new Products()
                {
                    Price = 250,
                    ProductCategory = new ProductCategory()
                    {
                        Name = "Kategória"
                    }
                },
                new Products()
                {
                    Price = 750,
                    ProductCategory = new ProductCategory()
                    {
                        Name = "Kategória"
                    }
                },
                new Products()
                {
                    Price = 10000,
                    ProductCategory = new ProductCategory()
                    {
                        Name = "Másik"
                    }
                }
            };
            repo.Setup(x => x.All()).Returns(products.AsQueryable());

            return repo;
        }

        /// <summary>
        /// Tests average price per category feature
        /// </summary>
        [Test]
        public void AveragePricePerCategory()
        {
            var repo = this.MockRepoFactory();
            var crepo = Mocked.CategoryRepo();

            using (ProductsLogic logic = new ProductsLogic(repo.Object, crepo.Object))
            {
                var result = logic.AveragePricePerCategory();

                Assert.That(result.Count(), Is.EqualTo(2));
                Assert.That(result.Single(x => x.Key == "Kategória").Value, Is.EqualTo(500));
                Assert.That(result.Single(x => x.Key == "Másik").Value, Is.EqualTo(10000));

                repo.Verify(x => x.All(), Times.Once);
                repo.VerifyNoOtherCalls();
                crepo.VerifyNoOtherCalls();
            }
        }

        /// <summary>
        /// Tests category translation
        /// </summary>
        [Test]
        public void GetCategory()
        {
            var repo = this.MockRepoFactory();
            var crepo = Mocked.CategoryRepo();

            using (ProductsLogic logic = new ProductsLogic(repo.Object, crepo.Object))
            {
                var result = logic.GetCategory(2);

                Assert.That(result, Is.EqualTo("Kategória 2"));

                repo.VerifyNoOtherCalls();
                crepo.Verify(x => x.All(), Times.Once);
                crepo.VerifyNoOtherCalls();
            }
        }

        /// <summary>
        /// Tests category option getter
        /// </summary>
        [Test]
        public void OptionsCategory()
        {
            var repo = this.MockRepoFactory();
            var crepo = Mocked.CategoryRepo();

            using (ProductsLogic logic = new ProductsLogic(repo.Object, crepo.Object))
            {
                var result = logic.OptionsCategory();

                Assert.That(result.Count, Is.EqualTo(2));
                Assert.That(result.Single(x => (int)x.Key == 1), Is.EqualTo(new KeyValuePair<object, object>(1, "Kategória 1")));

                repo.VerifyNoOtherCalls();
                crepo.Verify(x => x.All(), Times.Once);
                crepo.VerifyNoOtherCalls();
            }
        }
    }
}