﻿// <copyright file="StockLogicTests.cs" company="Apple Inc.">
// All rights reserved. © 2018
// </copyright>

namespace AppleStore.Logic.Tests
{
    using System.Collections.Generic;
    using System.Linq;
    using AppleStore.Data;
    using AppleStore.Logic;
    using AppleStore.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Tests for stock logic class
    /// </summary>
    [TestFixture]
    internal class StockLogicTests
    {
        /// <summary>
        /// Creates a mock repository
        /// </summary>
        /// <returns>Mocked repository</returns>
        public Mock<IRepository<Stock>> MockRepoFactory()
        {
            Mock<IRepository<Stock>> repo = new Mock<IRepository<Stock>>(MockBehavior.Loose);
            List<Stock> stocks = new List<Stock>
            {
                new Stock()
                {
                    Products = new Products()
                    {
                        Name = "Név",
                        Price = 100
                    },
                    Colors = new Colors()
                    {
                        Name = "Piros"
                    },
                    Storage = "64GB",
                    StoragePrice = new StoragePrice()
                    {
                        Price = 50
                    }
                }
            };
            repo.Setup(x => x.All()).Returns(stocks.AsQueryable());

            return repo;
        }

        /// <summary>
        /// Tests stock price functionality
        /// </summary>
        [Test]
        public void StockPrice()
        {
            var repo = this.MockRepoFactory();
            var prepo = Mocked.ProductsRepo();
            var crepo = Mocked.ColorsRepo();

            using (StockLogic logic = new StockLogic(repo.Object, prepo.Object, crepo.Object))
            {
                StockPriceHelper helper = logic.StockPrice().FirstOrDefault();

                Assert.That(helper.BasePrice, Is.EqualTo(100));
                Assert.That(helper.Price, Is.EqualTo(150));
                Assert.That(helper.Color, Is.EqualTo("Piros"));

                repo.Verify(x => x.All(), Times.Once);
                repo.VerifyNoOtherCalls();
                prepo.VerifyNoOtherCalls();
                crepo.VerifyNoOtherCalls();
            }
        }

        /// <summary>
        /// Tests color translation
        /// </summary>
        [Test]
        public void GetColor()
        {
            var repo = this.MockRepoFactory();
            var prepo = Mocked.ProductsRepo();
            var crepo = Mocked.ColorsRepo();

            using (StockLogic logic = new StockLogic(repo.Object, prepo.Object, crepo.Object))
            {
                var result = logic.GetColor(2);

                Assert.That(result, Is.EqualTo("Szín 2"));

                repo.VerifyNoOtherCalls();
                crepo.Verify(x => x.All(), Times.Once);
                crepo.VerifyNoOtherCalls();
                prepo.VerifyNoOtherCalls();
            }
        }

        /// <summary>
        /// Tests product translation
        /// </summary>
        [Test]
        public void GetProduct()
        {
            var repo = this.MockRepoFactory();
            var prepo = Mocked.ProductsRepo();
            var crepo = Mocked.ColorsRepo();

            using (StockLogic logic = new StockLogic(repo.Object, prepo.Object, crepo.Object))
            {
                var result = logic.GetProduct(2);

                Assert.That(result, Is.EqualTo("Termék 2"));

                repo.VerifyNoOtherCalls();
                prepo.Verify(x => x.All(), Times.Once);
                prepo.VerifyNoOtherCalls();
                crepo.VerifyNoOtherCalls();
            }
        }

        /// <summary>
        /// Tests color option getter
        /// </summary>
        [Test]
        public void OptionsColor()
        {
            var repo = this.MockRepoFactory();
            var prepo = Mocked.ProductsRepo();
            var crepo = Mocked.ColorsRepo();

            using (StockLogic logic = new StockLogic(repo.Object, prepo.Object, crepo.Object))
            {
                var result = logic.OptionsColor();

                Assert.That(result.Count, Is.EqualTo(2));
                Assert.That(result.Single(x => (int)x.Key == 1), Is.EqualTo(new KeyValuePair<object, object>(1, "Szín 1")));

                repo.VerifyNoOtherCalls();
                crepo.Verify(x => x.All(), Times.Once);
                crepo.VerifyNoOtherCalls();
                prepo.VerifyNoOtherCalls();
            }
        }

        /// <summary>
        /// Tests product option getter
        /// </summary>
        [Test]
        public void OptionsProduct()
        {
            var repo = this.MockRepoFactory();
            var prepo = Mocked.ProductsRepo();
            var crepo = Mocked.ColorsRepo();

            using (StockLogic logic = new StockLogic(repo.Object, prepo.Object, crepo.Object))
            {
                var result = logic.OptionsProduct();

                Assert.That(result.Count, Is.EqualTo(2));
                Assert.That(result.Single(x => (int)x.Key == 1), Is.EqualTo(new KeyValuePair<object, object>(1, "Termék 1")));

                repo.VerifyNoOtherCalls();
                prepo.Verify(x => x.All(), Times.Once);
                prepo.VerifyNoOtherCalls();
                crepo.VerifyNoOtherCalls();
            }
        }
    }
}
