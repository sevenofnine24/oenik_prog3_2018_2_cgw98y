﻿// <copyright file="MenuAction.cs" company="Apple Inc.">
// All rights reserved. © 2018
// </copyright>

namespace AppleStore.Menu
{
    using System;

    /// <summary>
    /// Menu item with a void type Action
    /// </summary>
    public class MenuAction : IMenuItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MenuAction"/> class.
        /// </summary>
        /// <param name="title">Title of item</param>
        /// <param name="action">Action to be invoked</param>
        public MenuAction(string title, Action action)
        {
            this.Title = title;
            this.Action = action;
        }

        /// <summary>
        /// Gets and sets action to be invoked
        /// </summary>
        public Action Action { get; private set; }

        /// <summary>
        /// Gets and sets title of item
        /// </summary>
        public string Title { get; private set; }

        /// <summary>
        /// Invokes without argument
        /// </summary>
        public void Invoke()
        {
            this.Action.Invoke();
        }
    }
}
