﻿// <copyright file="MenuClass.cs" company="Apple Inc.">
// All rights reserved. © 2018
// </copyright>

namespace AppleStore.Menu
{
    using System;

    /// <summary>
    /// Main menu class
    /// </summary>
    public class MenuClass
    {
        /// <summary>
        /// MenuSections of the menu
        /// </summary>
        private MenuSection[] sections;

        /// <summary>
        /// Currently active section
        /// </summary>
        private int activeSection = 0;

        /// <summary>
        /// Initializes a new instance of the <see cref="MenuClass"/> class.
        /// Loads sections into own property.
        /// </summary>
        /// <param name="sections">Sections to be loaded</param>
        /// <param name="title">Title of menu</param>
        /// <param name="subtitle">Subtitle of menu</param>
        public MenuClass(MenuSection[] sections, string title = null, string subtitle = null)
        {
            this.sections = sections;
            this.Title = title;
            this.Subtitle = subtitle;
        }

        /// <summary>
        /// Gets or sets the title of menu
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets subtitle of menu
        /// </summary>
        public string Subtitle { get; set; }

        /// <summary>
        /// Gets active section
        /// </summary>
        public MenuSection ActiveSection
        {
            get
            {
                return this.sections[this.activeSection];
            }
        }

        /// <summary>
        /// Starts the menu, handles button presses
        /// forwards to section if necessary
        /// </summary>
        /// <param name="exitToken">Exit token by reference</param>
        public void Start(ref bool exitToken)
        {
            Console.Clear();
            while (!exitToken && this.sections.Length > 0)
            {
                this.Render();
                ConsoleKeyInfo keyInfo = Console.ReadKey();
                switch (keyInfo.Key)
                {
                    case ConsoleKey.Escape:
                        exitToken = true;
                        break;
                    case ConsoleKey.DownArrow:
                        this.HandleMove(true);
                        break;
                    case ConsoleKey.UpArrow:
                        this.HandleMove(false);
                        break;
                    case ConsoleKey.LeftArrow:
                        this.ActiveSection.MoveLeft();
                        break;
                    case ConsoleKey.RightArrow:
                        this.ActiveSection.MoveRight();
                        break;
                    case ConsoleKey.Enter:
                        this.ActiveSection.Enter();
                        break;
                    case ConsoleKey.Tab:
                        this.SkipSection();
                        break;
                }
            }
        }

        /// <summary>
        /// Renders the section into console
        /// </summary>
        private void Render()
        {
            Console.SetCursorPosition(0, 0);

            // Writing title
            if (this.Title != null)
            {
                Console.BackgroundColor = ConsoleColor.DarkRed;
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine(this.Title);
            }

            // Writing subtitle
            if (this.Subtitle != null)
            {
                Console.BackgroundColor = ConsoleColor.Black;
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.WriteLine(this.Subtitle);
            }

            Console.WriteLine();
            for (int i = 0; i < this.sections.Length; i++)
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.BackgroundColor = ConsoleColor.DarkBlue;

                Console.WriteLine(this.sections[i].Title + "\n");
                this.sections[i].Render(i == this.activeSection);
                Console.WriteLine();
                Console.ResetColor();
            }
        }

        /// <summary>
        /// Handles edge cases
        /// like end of section
        /// in both directions
        /// </summary>
        /// <param name="positive">Positive or negative change</param>
        private void HandleMove(bool positive)
        {
            if (positive)
            {
                if (this.ActiveSection.LastSelected)
                {
                    if (this.activeSection < this.sections.Length - 1)
                    {
                        this.activeSection++;
                    }
                }
                else
                {
                    this.ActiveSection.MoveDown();
                }
            }
            else
            {
                if (this.ActiveSection.FirstSelected)
                {
                    if (this.activeSection > 0)
                    {
                        this.activeSection--;
                    }
                }
                else
                {
                    this.ActiveSection.MoveUp();
                }
            }
        }

        /// <summary>
        /// Skips to the next section, loops around to the beginning
        /// </summary>
        private void SkipSection()
        {
            if (this.activeSection < this.sections.Length - 1)
            {
                this.activeSection++;
            }
            else
            {
                this.activeSection = 0;
            }

            this.ActiveSection.ResetSelection();
        }
    }
}
