﻿// <copyright file="MenuFunction.cs" company="Apple Inc.">
// All rights reserved. © 2018
// </copyright>

namespace AppleStore.Menu
{
    using System;

    /// <summary>
    /// Item of menu that takes a single argument when invoked
    /// </summary>
    /// <typeparam name="T">Type of argument</typeparam>
    public class MenuFunction<T> : IMenuItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MenuFunction{T}"/> class.
        /// </summary>
        /// <param name="title">Title of item</param>
        /// <param name="action">Action to be invoked</param>
        /// <param name="argument">Argument for the action</param>
        public MenuFunction(string title, Action<T> action, T argument)
        {
            this.Title = title;
            this.Action = action;
            this.Argument = argument;
        }

        /// <summary>
        /// Gets and sets the action of menu item
        /// </summary>
        public Action<T> Action { get; private set; }

        /// <summary>
        /// Gets and sets title of menu item
        /// </summary>
        public string Title { get; private set; }

        /// <summary>
        /// Gets or sets argument for the action to be invoked with
        /// </summary>
        public T Argument { get; set; }

        /// <summary>
        /// Invokes action of item with argument
        /// TODO: exception for incorrect argument
        /// </summary>
        public void Invoke()
        {
            this.Action.Invoke(this.Argument);
        }
    }
}
