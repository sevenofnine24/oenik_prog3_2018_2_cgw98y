﻿// <copyright file="IMenuItem.cs" company="Apple Inc.">
// All rights reserved. © 2018
// </copyright>

namespace AppleStore.Menu
{
    /// <summary>
    /// Interface for all menu items
    /// </summary>
    public interface IMenuItem
    {
        /// <summary>
        /// Gets title of item
        /// </summary>
        string Title { get; }

        /// <summary>
        /// Invokes method of item
        /// </summary>
        void Invoke();
    }
}
