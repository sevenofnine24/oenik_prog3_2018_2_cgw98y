﻿// <copyright file="MenuChoice.cs" company="Apple Inc.">
// All rights reserved. © 2018
// </copyright>

namespace AppleStore.Menu
{
    /// <summary>
    /// To be used in multiple choice section
    /// </summary>
    public class MenuChoice : IMenuItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MenuChoice"/> class.
        /// </summary>
        /// <param name="title">Title of item</param>
        public MenuChoice(string title)
        {
            this.Title = title;
        }

        /// <inheritdoc />
        public string Title { get; private set; }

        /// <summary>
        /// No action, only used in multiple choice
        /// </summary>
        public void Invoke()
        {
            // empty method
        }
    }
}
