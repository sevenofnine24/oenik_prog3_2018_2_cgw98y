﻿// <copyright file="MenuSection.cs" company="Apple Inc.">
// All rights reserved. © 2018
// </copyright>

namespace AppleStore.Menu
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Holds IMenuItem elements
    /// </summary>
    public class MenuSection
    {
        /// <summary>
        /// Stores menu items
        /// </summary>
        private IMenuItem[] items;

        /// <summary>
        /// Stores page number
        /// </summary>
        private int page = 0;

        /// <summary>
        /// Stores limit of items to be displayed per page
        /// </summary>
        private int pagelimit;

        /// <summary>
        /// Selected item within section
        /// </summary>
        private int selected = 0;

        /// <summary>
        /// Is section is in select mode
        /// </summary>
        private bool multichoice;

        /// <summary>
        /// Selected items
        /// </summary>
        private bool[] choices;

        /// <summary>
        /// Initializes a new instance of the <see cref="MenuSection"/> class.
        /// </summary>
        /// <param name="title">Title of section</param>
        /// <param name="items">Item to be loaded</param>
        /// <param name="pagelimit">Number of items to be displayed per page</param>
        /// <param name="multichoice">Select multiple items</param>
        public MenuSection(string title, IMenuItem[] items, int pagelimit = 10, bool multichoice = false)
        {
            this.Title = title;
            this.items = items;
            this.pagelimit = pagelimit;
            this.multichoice = multichoice;

            if (multichoice)
            {
                this.choices = new bool[items.Length];
            }
        }

        /// <summary>
        /// Gets and sets the title of section
        /// </summary>
        public string Title { get; private set; }

        /// <summary>
        /// Gets number of items
        /// </summary>
        public int TrueLength
        {
            get
            {
                return this.items.Length - 1;
            }
        }

        /// <summary>
        /// Gets maximum number of pages
        /// </summary>
        public int MaxPages
        {
            get
            {
                return (int)Math.Ceiling((double)(this.items.Length / this.pagelimit));
            }
        }

        /// <summary>
        /// Gets a value indicating whether selected item is first on the page
        /// </summary>
        public bool FirstSelected
        {
            get
            {
                return this.selected == this.FirstSelection || this.selected == 0;
            }
        }

        /// <summary>
        /// Gets a value indicating whether selected item is last on the page
        /// </summary>
        public bool LastSelected
        {
            get
            {
                return this.selected == this.LastSelection || this.selected == this.TrueLength;
            }
        }

        /// <summary>
        /// Gets an enumerable of choices
        /// </summary>
        public IEnumerable<IMenuItem> Choices
        {
            get
            {
                for (int i = 0; i < this.items.Length; i++)
                {
                    if (this.choices[i])
                    {
                        yield return this.items[i];
                    }
                }
            }
        }

        /// <summary>
        /// Gets first selected item on page
        /// </summary>
        private int FirstSelection
        {
            get
            {
                return this.page * this.pagelimit;
            }
        }

        /// <summary>
        /// Gets last selected item on page
        /// </summary>
        private int LastSelection
        {
            get
            {
                return ((this.page + 1) * this.pagelimit) - 1;
            }
        }

        /// <summary>
        /// Moves selection up
        /// </summary>
        public void MoveUp()
        {
            this.selected--;
        }

        /// <summary>
        /// Moves selection down
        /// </summary>
        public void MoveDown()
        {
            this.selected++;
        }

        /// <summary>
        /// Moves a page left if possible
        /// </summary>
        public void MoveLeft()
        {
            Console.Clear();
            if (this.page > 0)
            {
                this.page--;
                this.selected = this.LastSelection;
            }
        }

        /// <summary>
        /// Moves a page right if possible
        /// </summary>
        public void MoveRight()
        {
            Console.Clear();
            if (this.page < this.MaxPages)
            {
                this.page++;
                this.selected = this.FirstSelection;
            }
        }

        /// <summary>
        /// Reloads menu with new items
        /// </summary>
        /// <param name="items">New items</param>
        public void Reload(IMenuItem[] items)
        {
            this.items = items;

            if (this.multichoice)
            {
                this.choices = new bool[items.Length];
            }
        }

        /// <summary>
        /// Renders section
        /// </summary>
        /// <param name="renderSelection">Passed from menu class, defines if section is active</param>
        public void Render(bool renderSelection)
        {
            int max = (this.page + 1) * this.pagelimit;
            if (max > this.items.Length)
            {
                max = this.items.Length;
            }

            for (int i = this.page * this.pagelimit; i < max; i++)
            {
                Console.ResetColor();
                if (renderSelection && i == this.selected)
                {
                    Console.ForegroundColor = ConsoleColor.Black;
                    Console.BackgroundColor = ConsoleColor.Gray;
                }

                if (this.multichoice)
                {
                    if (this.choices[i])
                    {
                        Console.Write(" x ");
                    }
                    else
                    {
                        Console.Write(" _ ");
                    }
                }

                Console.WriteLine(this.items[i].Title);
            }

            if (this.MaxPages > 0)
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.BackgroundColor = ConsoleColor.DarkGray;
                Console.WriteLine("\n{0}/{1}", this.page + 1, this.MaxPages + 1);
            }
        }

        /// <summary>
        /// Invokes the selected item
        /// </summary>
        public void Enter()
        {
            if (this.multichoice)
            {
                this.choices[this.selected] = !this.choices[this.selected];
            }
            else
            {
                Console.Clear();
                Console.WriteLine("Betöltés...");
                this.items[this.selected].Invoke();
                Console.Clear();
            }
        }

        /// <summary>
        /// Resets selection to 0
        /// </summary>
        public void ResetSelection()
        {
            this.selected = 0;
        }
    }
}
