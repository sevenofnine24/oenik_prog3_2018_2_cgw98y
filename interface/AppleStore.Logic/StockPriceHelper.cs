﻿// <copyright file="StockPriceHelper.cs" company="Apple Inc.">
// All rights reserved. © 2018
// </copyright>

namespace AppleStore.Logic
{
    /// <summary>
    /// Return type of stock full price feature
    /// </summary>
    public class StockPriceHelper
    {
        /// <summary>
        /// Gets or sets name of product
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets color of product
        /// </summary>
        public string Color { get; set; }

        /// <summary>
        /// Gets or sets storage of product
        /// </summary>
        public string Storage { get; set; }

        /// <summary>
        /// Gets or sets base price
        /// </summary>
        public int? BasePrice { get; set; }

        /// <summary>
        /// Gets or sets full price
        /// accounted for storage
        /// </summary>
        public int? Price { get; set; }
    }
}
