﻿// <copyright file="ProductsLogic.cs" company="Apple Inc.">
// All rights reserved. © 2018
// </copyright>

namespace AppleStore.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AppleStore.Data;
    using AppleStore.Repository;

    /// <summary>
    /// Additional logic for Products type
    /// </summary>
    public sealed class ProductsLogic : BaseLogic<Products>, ILogic<Products>
    {
        /// <summary>
        /// Repository object for ProductCategory
        /// </summary>
        private IRepository<ProductCategory> categoryrepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProductsLogic"/> class.
        /// Empty constructor
        /// only requires repo
        /// </summary>
        /// <param name="repo">Repository to be used</param>
        /// <param name="category">Category repository</param>
        public ProductsLogic(IRepository<Products> repo, IRepository<ProductCategory> category)
            : base(repo)
        {
            this.categoryrepo = category;
        }

        /// <inheritdoc />
        public string Title
        {
            get
            {
                return "Termékek";
            }
        }

        /// <inheritdoc />
        public string[] Headings
        {
            get
            {
                return new string[] { "Name", "Category", "Price", "ReleaseDate" };
            }
        }

        /// <inheritdoc />
        public string[] Properties
        {
            get
            {
                return this.Headings.Concat(new string[] { "Description" }).ToArray();
            }
        }

        /// <inheritdoc />
        public IEnumerable<KeyValuePair<object[], Products>> Formatted()
        {
            return this.Repository.All().ToList().Select(entity => new KeyValuePair<object[], Products>(
                new object[]
                {
                    entity.Name,
                    entity.ProductCategory.Name,
                    entity.Price + " Ft",
                    ((DateTime)entity.ReleaseDate).ToShortDateString()
                },
                entity));
        }

        /// <summary>
        /// Average price per category feature
        /// </summary>
        /// <returns>Enumerable of key value pairs</returns>
        public IEnumerable<KeyValuePair<string, int?>> AveragePricePerCategory()
        {
            return from product in this.Repository.All().ToList()
                   group product by product.ProductCategory.Name into g
                   select new KeyValuePair<string, int?>(g.Key, (int)g.Average(x => x.Price));
        }

        /// <summary>
        /// Gets category string from integer value
        /// </summary>
        /// <param name="value">Integer value</param>
        /// <returns>String representation</returns>
        public string GetCategory(int value)
        {
            return this.categoryrepo.All().Single(x => x.Id == (int)value).Name;
        }

        /// <summary>
        /// Options for Category field
        /// </summary>
        /// <returns>Dictionary of option => value</returns>
        public Dictionary<object, object> OptionsCategory()
        {
            return this.categoryrepo.All().ToDictionary(item => (object)item.Id, item => (object)item.Name);
        }

        /// <inheritdoc />
        public void Dispose()
        {
            this.Repository.Dispose();
            this.categoryrepo.Dispose();
        }
    }
}