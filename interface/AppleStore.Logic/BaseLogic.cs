﻿// <copyright file="BaseLogic.cs" company="Apple Inc.">
// All rights reserved. © 2018
// </copyright>

namespace AppleStore.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AppleStore.Repository;

    /// <summary>
    /// Base CRUD logic for all entity types
    /// </summary>
    /// <typeparam name="TEntity">Entity type</typeparam>
    public class BaseLogic<TEntity>
        where TEntity : class, new()
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BaseLogic{TEntity}"/> class.
        /// </summary>
        /// <param name="repo">Repository to be used</param>
        public BaseLogic(IRepository<TEntity> repo)
        {
            this.Repository = repo;
        }

        /// <summary>
        /// Gets or sets stored repository
        /// </summary>
        protected IRepository<TEntity> Repository { get; set; }

        /// <summary>
        /// Gets all entities
        /// </summary>
        /// <returns>All entities</returns>
        public virtual IQueryable<TEntity> All()
        {
            return this.Repository.All();
        }

        /// <summary>
        /// Saves entity
        /// </summary>
        /// <param name="entity">Entity to be modified</param>
        public void Save(TEntity entity)
        {
            this.Repository.Save(entity);
        }

        /// <summary>
        /// Add new entity
        /// </summary>
        /// <param name="entity">Entity to be added</param>
        public void Add(TEntity entity)
        {
            this.Repository.Add(entity);
        }

        /// <summary>
        /// Delete an entity
        /// </summary>
        /// <param name="entity">Entity to be deleted</param>
        public void Delete(TEntity entity)
        {
            this.Repository.Delete(entity);
        }
    }
}
