﻿// <copyright file="SumHelper.cs" company="Apple Inc.">
// All rights reserved. © 2018
// </copyright>

namespace AppleStore.Logic
{
    /// <summary>
    /// Return type of sum per customer feature
    /// </summary>
    public class SumHelper
    {
        /// <summary>
        /// Gets or sets customer name
        /// </summary>
        public string CustomerName { get; set; }

        /// <summary>
        /// Gets or sets customer address
        /// </summary>
        public string CustomerAddress { get; set; }

        /// <summary>
        /// Gets or sets sum of orders
        /// </summary>
        public int? Sum { get; set; }
    }
}
