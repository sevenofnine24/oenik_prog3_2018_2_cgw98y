﻿// <copyright file="ILogic.cs" company="Apple Inc.">
// All rights reserved. © 2018
// </copyright>

namespace AppleStore.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Interface for non basic logic types
    /// </summary>
    /// <typeparam name="TEntity">Entity type</typeparam>
    public interface ILogic<TEntity> : IDisposable
    {
        /// <summary>
        /// Gets the title of menu
        /// </summary>
        string Title { get; }

        /// <summary>
        /// Gets headings
        /// </summary>
        string[] Headings { get; }

        /// <summary>
        /// Gets properties
        /// </summary>
        string[] Properties { get; }

        /// <summary>
        /// Gets all entities
        /// </summary>
        /// <returns>All entities</returns>
        IQueryable<TEntity> All();

        /// <summary>
        /// Save changes to entity
        /// </summary>
        /// <param name="entity">Entity to be saved</param>
        void Save(TEntity entity);

        /// <summary>
        /// Add new entity
        /// </summary>
        /// <param name="entity">Entity to be added</param>
        void Add(TEntity entity);

        /// <summary>
        /// Delete an entity
        /// </summary>
        /// <param name="entity">Entity to be deleted</param>
        void Delete(TEntity entity);

        /// <summary>
        /// Formatted records
        /// </summary>
        /// <returns>Formatted enumerable</returns>
        IEnumerable<KeyValuePair<object[], TEntity>> Formatted();
    }
}
