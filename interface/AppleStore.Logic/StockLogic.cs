﻿// <copyright file="StockLogic.cs" company="Apple Inc.">
// All rights reserved. © 2018
// </copyright>

namespace AppleStore.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AppleStore.Data;
    using AppleStore.Repository;

    /// <summary>
    /// Logic for the Stock entity type
    /// </summary>
    public sealed class StockLogic : BaseLogic<Stock>, ILogic<Stock>
    {
        /// <summary>
        /// Repository object for Products
        /// </summary>
        private IRepository<Products> productsrepo;

        /// <summary>
        /// Repository object for Colors
        /// </summary>
        private IRepository<Colors> colorsrepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="StockLogic"/> class.
        /// Empty constructor
        /// only requires repo
        /// </summary>
        /// <param name="repo">Repository to be used</param>
        /// <param name="products">Products repository</param>
        /// <param name="colors">Colors repository</param>
        public StockLogic(IRepository<Stock> repo, IRepository<Products> products, IRepository<Colors> colors)
            : base(repo)
        {
            this.productsrepo = products;
            this.colorsrepo = colors;
        }

        /// <inheritdoc />
        public string Title
        {
            get
            {
                return "Raktár";
            }
        }

        /// <inheritdoc />
        public string[] Headings
        {
            get
            {
                return new string[] { "Product", "Color", "Storage", "Quantity", "ArrivalDate" };
            }
        }

        /// <inheritdoc />
        public string[] Properties
        {
            get
            {
                return this.Headings;
            }
        }

        /// <inheritdoc />
        public IEnumerable<KeyValuePair<object[], Stock>> Formatted()
        {
            return this.Repository.All().ToList().Select(entity => new KeyValuePair<object[], Stock>(
                new object[]
                {
                    entity.Products.Name,
                    entity.Colors.Name,
                    entity.StoragePrice.Storage,
                    entity.Quantity + "db",
                    ((DateTime)entity.ArrivalDate).ToShortDateString()
                },
                entity));
        }

        /// <summary>
        /// Gets stock with full prices
        /// </summary>
        /// <returns>Collections of StockPriceHelper objects</returns>
        public IQueryable<StockPriceHelper> StockPrice()
        {
            return from stock in this.Repository.All()
                   select new StockPriceHelper()
                   {
                       Name = stock.Products.Name,
                       Color = stock.Colors.Name,
                       Storage = stock.Storage,
                       BasePrice = stock.Products.Price,
                       Price = stock.Products.Price + stock.StoragePrice.Price
                   };
        }

        /// <summary>
        /// Gets color name by ID
        /// </summary>
        /// <param name="value">ID of color</param>
        /// <returns>Color name</returns>
        public string GetColor(int value)
        {
            return this.colorsrepo.All().Single(x => x.Id == value).Name;
        }

        /// <summary>
        /// Gets product name by ID
        /// </summary>
        /// <param name="value">ID of color</param>
        /// <returns>Product name</returns>
        public string GetProduct(int value)
        {
            return this.productsrepo.All().Single(x => x.Id == value).Name;
        }

        /// <summary>
        /// Gets color options
        /// </summary>
        /// <returns>Dictionary of options</returns>
        public Dictionary<object, object> OptionsColor()
        {
            return this.colorsrepo.All().ToDictionary(x => (object)x.Id, x => (object)x.Name);
        }

        /// <summary>
        /// Gets product options
        /// </summary>
        /// <returns>Dictionary of options</returns>
        public Dictionary<object, object> OptionsProduct()
        {
            return this.productsrepo.All().ToDictionary(x => (object)x.Id, x => (object)x.Name);
        }

        /// <inheritdoc />
        public void Dispose()
        {
            this.Repository.Dispose();
            this.colorsrepo.Dispose();
            this.productsrepo.Dispose();
        }
    }
}
