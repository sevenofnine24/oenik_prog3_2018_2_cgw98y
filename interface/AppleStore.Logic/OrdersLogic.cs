﻿// <copyright file="OrdersLogic.cs" company="Apple Inc.">
// All rights reserved. © 2018
// </copyright>

namespace AppleStore.Logic
{
    using System.Collections.Generic;
    using System.Linq;
    using AppleStore.Data;
    using AppleStore.Repository;

    /// <summary>
    /// Additional logic for Orders type
    /// </summary>
    public sealed class OrdersLogic : BaseLogic<Orders>, ILogic<Orders>
    {
        /// <summary>
        /// Logic object for Customers type
        /// </summary>
        private IRepository<Customers> customersrepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="OrdersLogic"/> class.
        /// Empty constructor
        /// only requires repo
        /// </summary>
        /// <param name="repo">Repository to be used</param>
        /// <param name="customers">Customers repository</param>
        public OrdersLogic(IRepository<Orders> repo, IRepository<Customers> customers)
            : base(repo)
        {
            this.customersrepo = customers;
        }

        /// <inheritdoc />
        public string Title
        {
            get
            {
                return "Rendelések";
            }
        }

        /// <inheritdoc />
        public string[] Headings
        {
            get
            {
                return new string[] { "Product", "Quantity", "PriceSum", "Customer" };
            }
        }

        /// <inheritdoc />
        public string[] Properties
        {
            get
            {
                return this.Headings;
            }
        }

        /// <inheritdoc />
        public IEnumerable<KeyValuePair<object[], Orders>> Formatted()
        {
            return this.Repository.All().ToList().Select(entity => new KeyValuePair<object[], Orders>(
                new object[]
                {
                    entity.Product,
                    entity.Quantity + "db",
                    entity.PriceSum + " Ft",
                    entity.Customers.Name
                },
                entity));
        }

        /// <summary>
        /// Sum per customer feature
        /// </summary>
        /// <returns>Collection of SumHelpers</returns>
        public IQueryable<SumHelper> SumPerCustomer()
        {
            return from order in this.Repository.All()
                   group order by new { order.Customers.Name, order.Customers.Address } into g
                   select new SumHelper()
                   {
                       CustomerName = g.Key.Name,
                       CustomerAddress = g.Key.Address,
                       Sum = g.Sum(x => x.PriceSum)
                   };
        }

        /// <summary>
        /// Gets customer string representation
        /// </summary>
        /// <param name="value">Id of customer</param>
        /// <returns>Name and address</returns>
        public string GetCustomer(int value)
        {
            Customers customer = this.customersrepo.All().Single(x => x.Id == value);
            return customer.Name + " - " + customer.Address;
        }

        /// <summary>
        /// Options for CustomerName
        /// </summary>
        /// <returns>String array of options</returns>
        public string[] OptionsCustomer()
        {
            return this.customersrepo.All().Select(x => x.Name + " - " + x.Address).ToArray();
        }

        /// <inheritdoc />
        public void Dispose()
        {
            this.Repository.Dispose();
            this.customersrepo.Dispose();
        }
    }
}
